package com.support.qualifications.getqualifications.database;

import android.provider.BaseColumns;

/**
 * データベースのカラム名一覧
 */
public final class DB_ColumnNames {

    /**
     * サーバ受信情報テーブル
     */
    public static abstract  class POST_DATA implements BaseColumns {

        /** 試験区分 */
        public static final String EXAM_CATEGORY = "exam_category";

        /** 問題ID */
        public static final String Q_ID = "question_id";

        /** 回答 */
        public static final String USER_ANSWER = "user_answer";

        /** 正誤 */
        public static final String CORRECT_MISTAKE = "correct_mistake";
    }

    /**
     * 問題情報テーブル
     */
    public static abstract class QUESTION_INFO implements BaseColumns {

        /** テーブル名 */
        public static final String TABLE_NAME = "tbl_question_info";

        /** 問題ID */
        public static final String Q_ID = "question_id";

        /** 年度 */
        public static final String YEAR = "year";

        /** 回次 */
        public static final String TURN = "turn";

        /** 問題番号 */
        public static final String Q_NO = "question_no";

        /** 分野コード */
        public static final String FIELD_CODE = "field_code";

        /** 大分類コード */
        public static final String L_CLASS_CODE = "large_class_code";

        /** 中分類コード */
        public static final String M_CLASS_CODE = "middle_class_code";

        /** 小分類コード */
        public static final String S_CLASS_CODE = "small_class_code";

        /** 問題名 */
        public static final String Q_NAME = "question_name";

        /** 問題文 */
        public static final String Q_SENTENCE = "question_sentence";

        /** 問題図 */
        public static final String Q_IMAGE = "question_image";

        /** 選択肢文ア */
        public static final String SELECTION_SENTENCE_A = "selection_sentence_a";

        /** 選択肢文イ */
        public static final String SELECTION_SENTENCE_I = "selection_sentence_i";

        /** 選択肢文ウ */
        public static final String SELECTION_SENTENCE_U = "selection_sentence_u";

        /** 選択肢文エ */
        public static final String SELECTION_SENTENCE_E = "selection_sentence_e";

        /** 選択肢図 */
        public static final String SELECTION_IMAGE = "selection_image";

        /** 解答 */
        public static final String CORRECT_ANSWER = "correct_answer";

        /** 問題正答率 */
        public static final String Q_CORRECT_RATE = "question_correct_rate";

        /** 間違い回数 */
        public static final String MISTAKE_COUNT = "mistake_count";

        /** お気に入りフラグ */
        public static final String FAVORITE_FLAG = "favorite_flag";

        /** 克服フラグ */
        public static final String OVERCOME_FLAG = "overcome_flag";

        /** 選択肢アの選択率 */
        public static final String SELECTION_RATE_A = "selection_rate_a";

        /** 選択肢イの選択率 */
        public static final String SELECTION_RATE_I = "selection_rate_i";

        /** 選択肢ウの選択率 */
        public static final String SELECTION_RATE_U = "selection_rate_u";

        /** 選択肢エの選択率 */
        public static final String SELECTION_RATE_E = "selection_rate_e";

        /** 選択肢？の選択率 */
        public static final String SELECTION_RATE_Q = "selection_rate_q";

    }

    /**
     * ジャンル情報テーブル
     */
    public static abstract class GENRE_INFO implements BaseColumns {

        /** テーブル名 */
        public static final String TABLE_NAME = "tbl_genre_info";

        /** 分野コード */
        public static final String FIELD_CODE = "field_code";

        /** 分野名 */
        public static final String FIELD_NAME = "field_name";

        /** 大分類コード */
        public static final String L_CLASS_CODE = "large_class_code";

        /** 大分類 */
        public static final String L_CLASS_NAME = "large_class_name";

        /** 中分類コード */
        public static final String M_CLASS_CODE = "middle_class_code";

        /** 中分類 */
        public static final String M_CLASS_NAME = "middle_class_name";

        /** 小分類コード */
        public static final String S_CLASS_CODE = "small_class_code";

        /** 小分類 */
        public static final String S_CLASS_NAME = "small_class_name";

        /** 小分類の正答率の分母 */
        public static final String S_CLASS_DENOMINATOR = "small_class_denominator";

        /** 小分類の正答率の分子 */
        public static final String S_CLASS_MOLECULE = "small_class_molecule";

        /** 小分類の正答率 */
        public static final String S_CLASS_CORRECT_RATE = "small_class_correct_rate";

        /** 中分類の正答率 */
        public static final String M_CLASS_CORRECT_RATE = "middle_class_correct_rate";

        /** 大分類の正答率 */
        public static final String L_CLASS_CORRECT_RATE = "large_class_correct_rate";

        /** 分野の正答率 */
        public static final String FIELD_CORRECT_RATE = "large_class_correct_rate";

    }

    /**
     * 回答履歴テーブル
     */
    public static abstract class ANSWER_ARCHIVE implements BaseColumns {

        /** テーブル名 */
        public static final String TABLE_NAME = "tbl_answer_archive";

        /** 回答ID */
        public static final String USER_ANSWER_ID = "user_answer_id";

        /** 問題ID */
        public static final String Q_ID = "question_id";

        /** 回答 */
        public static final String USER_ANSWER = "user_answer";

        /** 正誤 */
        public static final String CORRECT_MISTAKE = "correct_mistake";

        /** 回答日時 */
        public static final String USER_ANSWER_DATE = "user_answer_date";
    }

    /**
     * 回答履歴カウント
     */
    public static abstract class ANSWER_ARCHIVE_COUNT implements BaseColumns{

        /** カウントID */
        public static final String ID = "id_count";
    }

    /**
     * サーバ送信情報テーブル
     */
    public static abstract class SERVER_SEND_INFO implements BaseColumns {

        /** テーブル名 */
        public static final String TABLE_NAME = "tbl_server_send_info";

        /** 送信ID */
        public static final String SEND_ID = "send_id";

        /** 回答ID */
        public static final String USER_ANSWER_ID = "user_answer_id";

        /** 登録日時 */
        public static final String REGIST_DATE = "regist_date";
    }

    /**
     * サーバ送信情報カウント
     */
    public static abstract class SERVER_SEND_INFO_COUNT implements BaseColumns{

        /** カウントID */
        public static final String ID = "id_count";
    }

    /**
     * 成績情報テーブル
     */
    public static abstract class PERFORMANCE_INFO implements BaseColumns {

        /** テーブル名 */
        public static final String TABLE_NAME = "tbl_performance_info";

        /** 成績ID */
        public static final String PERFORMANCE_ID = "performance_id";

        /** 回答日時 */
        public static final String USER_ANSWER_DATE = "user_answer_date";

        /** 成績 */
        public static final String PERFORMANCE = "performance";

        /** 年度 */
        public static final String YEAR = "year";

        /** 回次 */
        public static final String TURN = "turn";
    }

    /**
     * 成績情報カウント
     */
    public static abstract class PERFORMANCE_INFO_COUNT implements BaseColumns{

        /** カウントID */
        public static final String ID = "id_count";
    }

    /**
     * 模擬試験情報テーブル
     */
    public static abstract class TRIAL_EXAM_INFO implements BaseColumns {

        /** テーブル名 */
        public static final String TABLE_NAME = "tbl_trial_exam_info";

        /** 出題ID */
        public static final String SET_Q_ID = "set_question_id";

        /** 問題ID */
        public static final String Q_ID = "question_id";

        /** 回答 */
        public static final String USER_ANSWER = "user_answer";

        /** 正誤 */
        public static final String CORRECT_MISTAKE = "correct_mistake";

        /** 回答日時 */
        public static final String USER_ANSWER_DATE = "user_answer_date";

        /** 経過時間 */
        public static final String ELAPSED_TIME = "elapsed_time";
    }
}

package com.support.qualifications.getqualifications.processing;

/**
 * 動的スクロールを行うための設定値を格納するクラス
 */
public class ScrollValue {
    private int Scrollposition;

    public int getScrollposition() {
        return Scrollposition;
    }

    public void setScrollposition(int scrollposition) {
        Scrollposition = scrollposition;
    }
}

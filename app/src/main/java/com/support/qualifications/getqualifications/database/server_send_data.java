package com.support.qualifications.getqualifications.database;

import java.io.Serializable;

/**
 * サーバに送信する情報のデータクラス
 */
public class server_send_data implements Serializable {

    /** 試験区分略称( AP,FE ) */
    private String exam_category;

    /** 問題ID( Q0001 ～ Q9999 ) */
    private String question_id;

    /** ユーザの回答( 'ア' ) */
    private String user_answer;

    /** 正誤( 1:正解,0:不正解 ) */
    private String correct_mistake;

    /**
     * 試験区分略称を取得
     * @return 試験区分略称( AP,FE )
     */
    public String getExam_category() {
        return exam_category;
    }

    /**
     * 試験区分略称を格納
     * @param exam_category 試験区分略称( AP,FE )
     */
    public void setExam_category(String exam_category) {
        this.exam_category = exam_category;
    }

    /**
     * 問題IDを取得
     * @return 問題ID( Q0001 ～ Q9999 )
     */
    public String getQuestion_id() {
        return question_id;
    }

    /**
     * 問題IDを格納
     * @param question_id 問題ID( Q0001 ～ Q9999 )
     */
    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    /**
     * 回答を取得
     * @return 回答( 'ア' )
     */
    public String getUser_answer() {
        return user_answer;
    }

    /**
     * 回答を格納
     * @param user_answer 回答( 'ア' )
     */
    public void setUser_answer(String user_answer) {
        this.user_answer = user_answer;
    }

    /**
     * 正誤を取得
     * @return 正誤 ( 1:正解,0:不正解 )
     */
    public String getCorrect_mistake() {
        return correct_mistake;
    }

    /**
     * 正誤を格納
     * @param correct_mistake 正誤( 1:正解,0:不正解 )
     */
    public void setCorrect_mistake(String correct_mistake) {
        this.correct_mistake = correct_mistake;
    }
}

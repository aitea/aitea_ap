package com.support.qualifications.getqualifications.processing;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.support.qualifications.getqualifications.fragment.Fragment_Question;
import com.support.qualifications.getqualifications.fragment.Fragment_Question_CorrectAnswer;

/**
 * 解説画面のタブを切り替える処理に用いるPageAdapter
 */
public class Question_Correct_Answer_PagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES = {"問題","解説検索"};
    private Bundle bundle;

    public Question_Correct_Answer_PagerAdapter(FragmentManager fm,Bundle bundle){
        super(fm);
        this.bundle = bundle;
    }
    /**
     * 指定ViewPagerの指定した場所のFragmentを取得する
     * @param viewPager 指定ViewPager
     * @param position 指定場所
     * @return 指定場所にあるフラグメントのインスタンス
     */
    public Fragment findFragmentByPosition(ViewPager viewPager, int position) {
        return (Fragment) instantiateItem(viewPager, position);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Fragment fragment1 = new Fragment_Question();
                fragment1.setArguments(bundle);
                return fragment1;
            case 1:
                Fragment fragment2 = new Fragment_Question_CorrectAnswer();
                fragment2.setArguments(bundle);
                return fragment2;
        }
        return null;
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }
}

package com.support.qualifications.getqualifications.database;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.BaseColumns;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * 模ジャンル情報テーブル(tbl_genre_info)のデータクラス
 */
public class genre_info_data implements Serializable {

    /** 分野コード( 1～* ) */
    private int field_code;

    /** 分野名( テクノロジ ) */
    private String field_name;

    /** 大分類コード( 1～** ) */
    private int l_class_code;

    /** 大分類名( 基礎理論 ) */
    private String l_class_name;

    /** 中分類コード( 1～** ) */
    private int m_class_code;

    /** 中分類名( ハードウェア ) */
    private String m_class_name;

    /** 小分類コード( 1～** ) */
    private int s_class_code;

    /** 小分類名( 離散数学 ) */
    private String s_class_name;

    //小分類の正答率のみ分母と分子を算出しているのは、
    //中分類と大分類と分野の正答率を算出するのに必要なデータだから

    /** 小分類の正答率の分母( 0.0～*.* ) */
    private double s_class_denominator;

    /** 小分類の正答率の分子( 0.0～*.* ) */
    private double s_class_molecule;

    /** 小分類の正答率( 0.000 ～ 1.000 ) */
    private double s_class_correct_rate;

    /** 中分類の正答率( 0.000 ～ 1.000 ) */
    private double m_class_correct_rate;

    /** 大分類の正答率( 0.000 ～ 1.000 ) */
    private double l_class_correct_rate;

    /** 分野の正答率( 0.000 ～ 1.000 ) */
    private double field_correct_rate;

    /**
     * 分野コードを取得
     * @return 分野コード( 1～* )
     */
    public int getField_code() {
        return field_code;
    }

    /**
     * 分野コードを格納
     * @param field_code 分野コード( 1～* )
     */
    public void setField_code(int field_code) {
        this.field_code = field_code;
    }

    /**
     * 分野名を取得
     * @return 分野名( テクノロジ )
     */
    public String getField_name() {
        return field_name;
    }


    /**
     * 分野名を格納
     * @param field_name 分野名( テクノロジ )
     */
    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    /**
     * 大分類コードを取得
     * @return 大分類コード( 1～** )
     */
    public int getL_class_code() {
        return l_class_code;
    }

    /**
     * 大分類コードを格納
     * @param l_class_code 大分類コード( 1～** )
     */
    public void setL_class_code(int l_class_code) {
        this.l_class_code = l_class_code;
    }

    /**
     * 大分類名を取得
     * @return 大分類名( 基礎理論 )
     */
    public String getL_class_name() {
        return l_class_name;
    }

    /**
     * 大分類名を格納
     * @param l_class_name 大分類名( 基礎理論 )
     */
    public void setL_class_name(String l_class_name) {
        this.l_class_name = l_class_name;
    }

    /**
     * 中分類コードを取得
     * @return 中分類コード( 1～** )
     */
    public int getM_class_code() {
        return m_class_code;
    }

    /**
     * 中分類コードを格納
     * @param m_class_code 中分類コード( 1～** )
     */
    public void setM_class_code(int m_class_code) {
        this.m_class_code = m_class_code;
    }

    /**
     * 中分類名を取得
     * @return 中分類名( ハードウェア )
     */
    public String getM_class_name() {
        return m_class_name;
    }

    /**
     * 中分類名を格納
     * @param m_class_name 中分類名( ハードウェア )
     */
    public void setM_class_name(String m_class_name) {
        this.m_class_name = m_class_name;
    }

    /**
     * 小分類コードを取得
     * @return 小分類コード( 1～** )
     */
    public int getS_class_code() {
        return s_class_code;
    }

    /**
     * 小分類コードを格納
     * @param s_class_code 小分類コード( 1～** )
     */
    public void setS_class_code(int s_class_code) {
        this.s_class_code = s_class_code;
    }

    /**
     * 小分類名を取得
     * @return 小分類名( 離散数学 )
     */
    public String getS_class_name() {
        return s_class_name;
    }

    /**
     * 小分類名を格納
     * @param s_class_name 小分類名( 離散数学 )
     */
    public void setS_class_name(String s_class_name) {
        this.s_class_name = s_class_name;
    }

    /**
     * 小分類の正答率の分母を取得
     * @return 小分類の正答率の分母( 0.0～*.* )
     */
    public double getS_class_denominator() {
        return s_class_denominator;
    }

    /**
     * 小分類の正答率の分母を格納
     * @param s_class_denominator 小分類の正答率の分母( 0.0～*.* )
     */
    public void setS_class_denominator(double s_class_denominator) {
        this.s_class_denominator = s_class_denominator;
    }

    /**
     * 小分類の正答率の分子を取得
     * @return 小分類の正答率の分子( 0.0～*.* )
     */
    public double getS_class_molecule() {
        return s_class_molecule;
    }

    /**
     * 小分類の正答率の分子を格納
     * @param s_class_molecule 小分類の正答率の分子( 0.0～*.* )
     */
    public void setS_class_molecule(double s_class_molecule) {
        this.s_class_molecule = s_class_molecule;
    }

    /**
     * 小分類の正答率を取得
     * @return 小分類の正答率( 0.000 ～ 1.000 )
     */
    public double getS_class_correct_rate() {
        return s_class_correct_rate;
    }

    /**
     * 小分類の正答率を格納
     * @param s_class_correct_rate 小分類の正答率( 0.000 ～ 1.000 )
     */
    public void setS_class_correct_rate(double s_class_correct_rate) {
        this.s_class_correct_rate = s_class_correct_rate;
    }

    /**
     * 中分類の正答率を取得
     * @return 中分類の正答率( 0.000～1.000 )
     */
    public double getM_class_correct_rate() {
        return m_class_correct_rate;
    }

    /**
     * 中分類の正答率を格納
     * @param m_class_correct_rate 中分類の正答率( 0.000～1.000 )
     */
    public void setM_class_correct_rate(double m_class_correct_rate) {
        this.m_class_correct_rate = m_class_correct_rate;
    }

    /**
     * 大分類の正答率を取得
     * @return 大分類の正答率( 0.000～1.000 )
     */
    public double getL_class_correct_rate() {
        return l_class_correct_rate;
    }

    /**
     * 大分類の正答率を格納
     * @param l_class_correct_rate 大分類の正答率( 0.000～1.000 )
     */
    public void setL_class_correct_rate(double l_class_correct_rate) {
        this.l_class_correct_rate = l_class_correct_rate;
    }

    /**
     * 分野の正答率を取得
     * @return 分野の正答率( 0.000～1.000 )
     */
    public double getField_correct_rate() {
        return field_correct_rate;
    }

    /**
     * 分野の正答率を格納
     * @param field_correct_rate 分野の正答率( 0.000～1.000 )
     */
    public void setField_correct_rate(double field_correct_rate) {
        this.field_correct_rate = field_correct_rate;
    }
}
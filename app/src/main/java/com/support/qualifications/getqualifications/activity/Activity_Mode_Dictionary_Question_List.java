// 参考URL http://ichitcltk.hustle.ne.jp/gudon2/index.php?pageType=file&id=Android028_ListView1

package com.support.qualifications.getqualifications.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapText;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.AiteaMode;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.question_info_data;
import com.support.qualifications.getqualifications.processing.ScrollValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 * 問題リスト画面
 */
public class Activity_Mode_Dictionary_Question_List extends Activity_Base implements CompoundButton.OnCheckedChangeListener{

    /** モードID */
    private int mode_id;

    /** リストビューのスクロール位置 */
    public static ScrollValue scrollValue =  new ScrollValue();

    /** ランダムモード(true:ランダムモード,false:通常モード) */
    private boolean isRandomMode;

    /** 問題IDリスト */
    private String[] qid_data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary_question_list);

        //自動スクロール位置初期化
        scrollValue.setScrollposition(0);

        //ナビゲーションドロワーセット
        setNavigation((Toolbar)findViewById(R.id.toolbar_mode_dictionary_question_list));

        //intentからデータ取得
        getIntentValue();

        //ランダムスイッチにチェンジリスナーをセット
        ((Switch)findViewById(R.id.random_switch)).setOnCheckedChangeListener(this);

        //問題IDリストが存在する場合
        if( qid_data != null ) {
            //ListViewセット
            setListView(getUsers());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //今まで解いていた問題の場所まで自動スクロール
        if(!isRandomMode) {
            ((ListView) findViewById(R.id.listview_dictionary_question_list)).smoothScrollToPositionFromTop(scrollValue.getScrollposition(), 0, 500);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * Intentからデータを取得する
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
        ((TextView)findViewById(R.id.question_list_title)).setText(intent.getStringExtra(IntentKeyword.TOOLBER_TITLE));
        qid_data = intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);

    }
    /**
     * ListViewに表示するデータの定義クラス(User)のArrayListを作成し
     * その結果を返す
     * @return UserのArrayList
     */
    private ArrayList<User> getUsers(){

        ArrayList<User> users = new ArrayList<>();

        //アイコン画像のid
        int icons = 0;

        //リストViewを表示するために必要な情報を取得
        question_info_data[] q_i_data = get_show_data(qid_data);

        for (int i = 0; i < q_i_data.length; i++) {
            User user = new User();
            user.setIcon(BitmapFactory.decodeResource(getResources(), icons));
            user.setQ_info(getQ_info(q_i_data[i]));
            user.setQ_summary(q_i_data[i].getQ_name());
            users.add(user);
        }
        return users;
    }

    /**
     * レストビューの問題情報として表示する文字列を取得する
     * モードによって取得する情報を変化させる
     *
     * @param q_i_data 問題情報
     * @return リストビューに表示する問題情報
     */
    private String getQ_info(question_info_data q_i_data){
        switch (mode_id){
            case AiteaMode.GENRE:
                return q_i_data.getYear() + " " + q_i_data.getTurn() + " 問 " + String.format("%02d", q_i_data.getQ_no());
            case AiteaMode.YEAR:
                return "問 " + String.format("%02d",q_i_data.getQ_no());
            default:
                return null;
        }
    }

    /**
     * ListViewの設定を行う
     * @param users　ListViewに表示するデータ
     */
    private void setListView( ArrayList<User> users ){

        UserAdapter adapter = new UserAdapter(this, 0, users);
        final ListView myListview = (ListView)findViewById(R.id.listview_dictionary_question_list);
        myListview.setEmptyView(findViewById(R.id.empty));
        myListview.setAdapter(adapter);
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {
                //選択した問題回答画面へ
                goSelectQuestionActivity(position);
            }
        });
    }
    /**
     * 問題IDリストをシャッフルする
     * 選択した問題の問題IDは先頭に配置する
     * @param position 選択した問題の場所
     * @return シャッフルした問題IDリスト
     */
    private String[] getShuffle_qid_data(int position){
        //配列からlistへ変換
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(qid_data));
        //選択した問題のq_idを退避
        String first_q_id = list.get(position);
        list.remove(position);
        //リストの並びをシャッフル
        Collections.shuffle(list);
        //退避したq_idを先頭に追加
        list.add(0, first_q_id);
        //シャッフルしたデータを返す
        return (String[])list.toArray(new String[list.size()]);
    }

    /**
     * ListViewで選択した問題の回答画面に画面遷移する処理を行う
     * @param position 選択した問題の場所
     */
    private void goSelectQuestionActivity(int position){

        Intent intent = new Intent(Activity_Mode_Dictionary_Question_List.this, Activity_Question.class);

        intent.putExtra(IntentKeyword.MODE_ID, mode_id);

        //ランダムモード
        if(isRandomMode) {
            intent.putExtra(IntentKeyword.SELECT_Q_ID, 0);
            //シャッフルした問題IDリストを渡す
            intent.putExtra(IntentKeyword.Q_ID_LIST, getShuffle_qid_data(position));
        }else{
            intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
            intent.putExtra(IntentKeyword.Q_ID_LIST, qid_data);
        }
        startActivity(intent);
    }


    /**
     * 指定問題IDリストの問題情報リストを取得する
     * @param q_id_data 問題IDリスト
     * @return 問題情報リスト
     */
    private question_info_data[] get_show_data(String[] q_id_data){

        //全ての問題の情報をハッシュマップで取得
        HashMap<String,question_info_data> q_i_data_list = get_all_list_q_id_info();

        question_info_data[] out_q_i_data = new question_info_data[q_id_data.length];

        for( int i = 0; i < q_id_data.length; i++ ){
            out_q_i_data[i] = q_i_data_list.get(q_id_data[i]);
        }

        return out_q_i_data;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(isChecked == true){
            //ランダムモードon
            isRandomMode = true;
        }else{
            //ランダムモードoff
            isRandomMode = false;
        }
    }

    /**
     * 全ての問題IDを取得し、問題IDをキーに問題情報を取得できるハッシュマップを返す
     * @return 問題IDをキーに問題情報を取得できるハッシュマップ
     */
    private HashMap<String,question_info_data> get_all_list_q_id_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        HashMap<String,question_info_data> map = new HashMap<>();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_all_list_q_id_info(), null);
        while (c.moveToNext()) {
            // keyをつくる
            String key = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            question_info_data questionInfoData = new question_info_data();
            questionInfoData.setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            questionInfoData.setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            questionInfoData.setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
            questionInfoData.setQ_name(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NAME)));
            map.put(key, questionInfoData);
        }
        c.close();
        db.close();
        return map;
    }

    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            if( convertview == null ){

                //モードによってリストアイテムを切り替える
                switch (mode_id){
                    case AiteaMode.YEAR:
                        convertview = layoutInflater.inflate(R.layout.question_list_item_year, parent, false);
                        break;
                    case AiteaMode.GENRE:
                        convertview = layoutInflater.inflate(R.layout.question_list_item_genre, parent, false);
                        break;
                }

                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.questionicon);
                holder.info = (com.beardedhen.androidbootstrap.BootstrapLabel) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }
            User user = (User)getItem(pos);
            holder.icon.setImageBitmap(user.getIcon());
            holder.info.setBootstrapText(new BootstrapText.Builder(getContext()).addText(user.getQ_info()).build());
            holder.summary.setText(user.getQ_summary());
            return convertview;

        }
    }

    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** アイコン画像 */
        ImageView icon;

        /** 問題情報( 問1など )を表示するテキスト */
        com.beardedhen.androidbootstrap.BootstrapLabel info;

        /** 問題名を表示するテキスト */
        TextView summary;
    }

    /**
     * ListViewに表示するデータの定義クラス
     */
    private class User{

        /** アイコン画像 */
        private Bitmap icon;

        /** 問題情報( 問1など ) */
        private String q_info;

        /** 問題名 */
        private String q_summary;

        /**
         * アイコン画像を格納する
         * @param icon アイコン画像
         */
        public void setIcon(Bitmap icon) {
            this.icon = icon;
        }

        /**
         * 問題情報を格納する
         * @param q_info 問題情報( 問1 平成**年度 *期 )
         */
        public void setQ_info(String q_info) {
            this.q_info = q_info;
        }

        /**
         * 問題名を格納する
         * @param q_summary 問題名
         */
        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        /**
         * アイコン画像を取得する
         * @return アイコン画像
         */
        public Bitmap getIcon() {

            return icon;
        }

        /**
         * 問題情報を取得する
         * @return 問題情報
         */
        public String getQ_info() {
            return q_info;
        }

        /**
         * 問題名を取得する
         * @return 問題名
         */
        public String getQ_summary() {
            return q_summary;
        }
    }


}

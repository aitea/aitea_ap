package com.support.qualifications.getqualifications.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.ads.AdView;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.AiteaMode;
import com.support.qualifications.getqualifications.common.Date;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.question_info_data;
import com.support.qualifications.getqualifications.fragment.Fragment_Question_CorrectAnswer;
import com.support.qualifications.getqualifications.processing.Question_Correct_Answer_PagerAdapter;

/**
 * 問題の正答表示と解説表示画面
 */
public class Activity_Question_CorrectAnswer extends Activity_Base{

    /** モードID */
    private int mode_id;

    /** 問題IDリスト */
    private String[] q_id_data;

    /** 問題IDリスト内の現在の添え字( 0 ～ q_id_data.length - 1 ) */
    private int currentId;

    /** ユーザの回答履歴
     *  模擬試験モードで使用する
     */
    private String[] useranswers;

    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_correctanswer);

        //広告セット
        setAdView((AdView) findViewById(R.id.adView_correctAnswer));

        Intent intent = getIntent();

        //intentから受け取った値をメンバ変数に反映
        getIntentValue(intent);

        String useranswer = getUserAnswer(intent);

        //問題IDリストから、この画面を表示するのに必要な問題情報を得る
        question_info_data q_i_data = get_correctAnswer_info(q_id_data[currentId]);

        ((TextView) findViewById(R.id.user_answer)).setText("あなたの回答：" + useranswer);

        //回答した問題の正解を取得
        String correctanswer = q_i_data.getCorrect_answer();
        ((TextView) findViewById(R.id.correct_answer)).setText("正解　　　　：" + correctanswer);

        int correct_mistake = getCorrectMistake(useranswer, correctanswer);

        if( mode_id != AiteaMode.TRIALEXAM ){
            if( correct_mistake == 0 ) {
                increment_mistake_count(q_id_data[currentId]);
            }

            //回答履歴テーブルとサーバ送信テーブルにレコードを追加
            addRecordToDatabases(q_id_data[currentId],useranswer, correct_mistake);
        }
        
        setToolbar(q_i_data);

        //タブの設定
        setTabs();


    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * タブをセットする
     */
    private void setTabs(){
        pager = (ViewPager) findViewById(R.id.pager);
        Bundle bundle = new Bundle();
        bundle.putString(IntentKeyword.CURRENT_Q_ID, q_id_data[currentId]);

        pager.setAdapter(new Question_Correct_Answer_PagerAdapter(getSupportFragmentManager(),bundle));

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(pager);
    }

    /**
     * お気に入り克服のアイコンをセットする
     * @param q_id 問題ID
     */
    private void setFlagIcon(String q_id) {
        if (getFavoriteFlag(q_id)) {
            ((Toolbar) findViewById(R.id.toolbar_question_correctanswer)).getMenu().findItem(R.id.Q_favorite).setIcon(R.drawable.icon_favorite_true);
        }else{
            ((Toolbar) findViewById(R.id.toolbar_question_correctanswer)).getMenu().findItem(R.id.Q_favorite).setIcon(R.drawable.icon_favorite_false);
        }
        if(getOvercomeFlag(q_id)) {
            ((Toolbar) findViewById(R.id.toolbar_question_correctanswer)).getMenu().findItem(R.id.Q_overcome).setIcon(R.drawable.icon_overcome_true);
        }else{
            ((Toolbar) findViewById(R.id.toolbar_question_correctanswer)).getMenu().findItem(R.id.Q_overcome).setIcon(R.drawable.icon_overcome_false);
        }
    }
    /**
     * 戻るボタンを押下した際の処理
     * @return Return <code>true</code> to prevent this event from being propagated
     * further, or <code>false</code> to indicate that you have not handled
     * this event and it should continue to be propagated.
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // Keyボタン(BACKボタン)押下時の処理
        if(keyCode == KeyEvent.KEYCODE_BACK) {

            setScrollPosition();

            //Webページのほうのタブを開いている
            if (pager.getCurrentItem() == 1) {
                //前のページに戻る
                boolean goBackIsSuccess = ((Fragment_Question_CorrectAnswer) ((Question_Correct_Answer_PagerAdapter) pager.getAdapter()).findFragmentByPosition(pager, 1)).goBackMyWebView();
                //戻り切っていたら
                if(!goBackIsSuccess){
                    return super.onKeyDown(keyCode, event);
                }
            }else{
                return super.onKeyDown(keyCode, event);
            }

        }
        return false;
    }

    /**
     * 現在の問題の位置を問題リストのスクロール位置として設定する
     */
    private void setScrollPosition(){
        switch (mode_id) {
            case AiteaMode.GENRE:
            case AiteaMode.YEAR:
                Activity_Mode_Dictionary_Question_List.scrollValue.setScrollposition(currentId);
            case AiteaMode.TRIALEXAM:
                Activity_Mode_TrialExam_List.scrollValue.setScrollposition(currentId);
            case AiteaMode.REVIEW:
                Activity_Mode_Review.scrollValue.setScrollposition(currentId);
            default:
                break;
        }
    }

    /**
     * Intentで渡されたデータを取得する
     * @param intent 前画面から受け取ったIntent
     */
    private void getIntentValue(Intent intent){
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
        currentId = intent.getIntExtra(IntentKeyword.SELECT_Q_ID, 0);
        q_id_data = intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);
    }


    /**
     * ユーザの回答と正解を照らし合わせ正誤を判定する。
     * 正解の場合は「1」、誤りの場合は「0」を返す
     * @param useranswer ユーザの回答
     * @param correctanswer ユーザが回答した問題の正解
     * @return 正解の場合は「1」、誤りの場合は「0」
     */
    private int getCorrectMistake( String useranswer , String correctanswer ){

        //各リストアイテムの左に表示するアイコンを取得
        ImageView correct_mistakeImage = (ImageView) findViewById(R.id.correct_mistake_image);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.correct_answer_LinearLayout);

        if (useranswer.equals(correctanswer)) {
            correct_mistakeImage.setImageResource(R.drawable.icon_maru);
            linearLayout.setBackgroundColor(getResources().getColor(R.color.PeachPuff));
            return 1;
        } else {
            correct_mistakeImage.setImageResource(R.drawable.icon_batsu);
            linearLayout.setBackgroundColor(getResources().getColor(R.color.PaleTurquoise));
            return 0;
        }
    }

    /**
     * 回答履歴テーブルとサーバ送信情報テーブルにレコードを追加する
     * 追加する情報は、回答時刻、正誤、ユーザの回答内容
     * @param useranswer 回答内容
     * @param correct_mistake 正誤
     */
    private void addRecordToDatabases( String q_id , String useranswer , int correct_mistake ){

        //現在時刻を取得
        String nowDate = Date.getNowDate();

        //現在データベース内の最大の回答IDに+1した回答IDを取得
        String next_answer_id = "A" + String.format("%05d", get_user_answer_id_count() + 1) ;

        add_answer_archive_info(next_answer_id, q_id, useranswer, correct_mistake, nowDate);

        //現在データベース内の最大の送信IDに+1した送信IDを取得
        String next_send_id = "S" +  String.format("%03d", get_send_id_count() + 1) ;

        add_server_send_info(next_send_id, next_answer_id, nowDate);

    }

    /**
     * 回答履歴情報のレコード数を取得する
     * @return レコード数
     */
    private int get_user_answer_id_count(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int answer_id_count = 0;

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_ANSWER_ARCHIVE.get_user_answer_id_count(), null);

        if (c.moveToNext()) {
            answer_id_count = c.getInt(c.getColumnIndex(DB_ColumnNames.ANSWER_ARCHIVE_COUNT.ID));
        }

        c.close();
        db.close();

        return answer_id_count;
    }

    /**
     * サーバ送信情報のレコード数を取得する
     * @return レコード数
     */
    private int get_send_id_count(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int send_id_count = 0;

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_SERVER_SEND_INFO.get_send_id_count(), null);

        if (c.moveToNext()) {
            send_id_count = c.getInt(c.getColumnIndex(DB_ColumnNames.SERVER_SEND_INFO_COUNT.ID));
        }

        c.close();
        db.close();

        return send_id_count;
    }

    /**
     * 回答履歴テーブルにレコードを追加する
     * @param user_answer_id	回答ID
     * @param q_id				問題ID
     * @param user_answer		回答
     * @param correct_mistake	正誤
     * @param user_answer_date	回答日
     */
    private void add_answer_archive_info(String user_answer_id,String q_id,String user_answer,int correct_mistake , String user_answer_date){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.execSQL(DB_SqlMaker.TBL_ANSWER_ARCHIVE.add_answer_archive_info(user_answer_id, q_id, user_answer, correct_mistake, user_answer_date));
        db.close();
    }

    /**
     * サーバ送信情報にレコードを追加する
     * @param send_id			送信ID
     * @param user_answer_id	回答ID
     * @param regist_date		登録日時
     */
    private void add_server_send_info(String send_id,String user_answer_id, String regist_date){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.execSQL(DB_SqlMaker.TBL_SERVER_SEND_INFO.add_server_send_info(send_id, user_answer_id, regist_date));
        db.close();
    }

    /**
     * 間違い回数をインクリメントする
     * @param q_id	問題ID
     */
    private void increment_mistake_count(String q_id){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.increment_mistake_count(q_id));
        db.close();
    }
    /**
     * ユーザの回答内容を取得する
     * @param intent 前画面から受け取ったIntent
     * @return
     */
    private String getUserAnswer( Intent intent ){

        //模擬試験モードのときは、回答履歴を参照する
        if( mode_id == AiteaMode.TRIALEXAM ) {
            useranswers = intent.getStringArrayExtra(IntentKeyword.SELECT_ANSWER_LIST);
            return useranswers[currentId];
        }
        else{
            return intent.getStringExtra(IntentKeyword.USER_ANSWER);
        }
    }

    /**
     * ツールバーの設定を行う
     */
    private void setToolbar(question_info_data q_i_data){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_question_correctanswer);
        setNavigation(toolbar);
        ((TextView)findViewById(R.id.question_correctanswer_title)).setText(q_i_data.getYear() + " " + q_i_data.getTurn() + " 問" + q_i_data.getQ_no());
        toolbar.inflateMenu(R.menu.question);
        setFlagIcon(q_id_data[currentId]);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    // メニュー内の"お気に入り"押下後の処理
                    case R.id.Q_favorite:
                        if (getFavoriteFlag(q_id_data[currentId])) {
                            item.setIcon(R.drawable.icon_favorite_false);
                            setFavoriteFlag(q_id_data[currentId], false);
                        } else {
                            item.setIcon(R.drawable.icon_favorite_true);
                            setFavoriteFlag(q_id_data[currentId], true);
                        }
                        break;

                    // メニュー内の"克服"押下後の処理
                    case R.id.Q_overcome:
                        if (getOvercomeFlag(q_id_data[currentId])) {
                            item.setIcon(R.drawable.icon_overcome_false);
                            setOvercomeFlag(q_id_data[currentId], false);
                        } else {
                            item.setIcon(R.drawable.icon_overcome_true);
                            setOvercomeFlag(q_id_data[currentId], true);
                            //アラート表示
                            ClickOvercomeAlertShow();
                        }
                        break;

                    // メニュー内の"右矢印"押下後の処理
                    case R.id.menu_nextquestiion:

                        //問題リストの最後の問題だった場合
                        if ((currentId + 1) == q_id_data.length) {
                            //現在の問題の位置を渡す
                            Activity_Mode_Dictionary_Question_List.scrollValue.setScrollposition(currentId);
                            finish();
                        } else {
                            //次の画面へ
                            goNextActivity();
                        }
                        break;
                }
                return true;
            }
        });
    }

    /**
     * 次の画面に遷移する処理
     */
    private void goNextActivity(){

        //模擬試験モード
        if (mode_id == AiteaMode.TRIALEXAM) {
            //解説画面へ
            goActivity_Question_CorrectAnswer();
        } else {
            //次の問題へ
            goNextActivity_Question();
        }
    }

    /**
     * 解説画面へ遷移する処理
     */
    private void goActivity_Question_CorrectAnswer(){

        Intent intent = new Intent(Activity_Question_CorrectAnswer.this, Activity_Question_CorrectAnswer.class);
        intent.putExtra(IntentKeyword.MODE_ID, mode_id);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, currentId + 1);
        intent.putExtra(IntentKeyword.SELECT_ANSWER_LIST, useranswers);
        startActivity(intent);
        finish();

    }

    /**
     * 次の問題へ遷移する処理
     */
    private void goNextActivity_Question(){

        Intent intent = new Intent(Activity_Question_CorrectAnswer.this, Activity_Question.class);
        intent.putExtra(IntentKeyword.MODE_ID, mode_id);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, currentId + 1);
        startActivity(intent);
        finish();

    }

    /**
     * 引数で指定した問題IDに該当する、解答画面表示に必要な情報を表示する
     * @param q_id 問題ID
     * @return 解答画面表示に必要な情報
     */
    private question_info_data get_correctAnswer_info(String q_id) {

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        question_info_data out_q_i_data = new question_info_data();

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_correctAnswer_info(q_id), null);

        if (c.moveToNext()) {
                out_q_i_data.setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
                out_q_i_data.setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
                out_q_i_data.setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
                out_q_i_data.setCorrect_answer(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.CORRECT_ANSWER)));
        }
        c.close();
        db.close();

        return out_q_i_data;
    }
    /**
     * 指定問題IDのお気に入りフラグを設定する
     * @param q_id 指定問題ID
     * @param flag 設定値
     */
    private void setFavoriteFlag(String q_id, boolean flag){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        if(flag) {
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_favorite_flag(q_id,1));
        }else{
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_favorite_flag(q_id, 0));
        }

        db.close();
    }

    /**
     * 指定問題IDのお気に入りフラグを取得する
     * @param q_id 問題ID
     * @return お気に入りフラグ
     */
    private boolean getFavoriteFlag(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_favorite_flag(q_id), null);

        if (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG)) == 1){
                c.close();
                db.close();
                return true;
            }
        }
        c.close();
        db.close();
        return false;
    }

    /**
     * 指定問題IDの克服フラグを設定する
     * @param q_id 指定問題ID
     * @param flag 設定値
     */
    private void setOvercomeFlag(String q_id, boolean flag){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        if(flag) {
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_overcome_flag(q_id, 1));
        }else{
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_overcome_flag(q_id, 0));
        }

        db.close();
    }

    /**
     * 指定問題IDの克服フラグを取得する
     * @param q_id 問題ID
     * @return 克服フラグ
     */
    private boolean getOvercomeFlag(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_overcome_flag(q_id), null);

        if (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG)) == 1){
                c.close();
                db.close();
                return true;
            }
        }
        c.close();
        db.close();
        return false;
    }

}

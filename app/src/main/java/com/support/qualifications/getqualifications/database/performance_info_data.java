package com.support.qualifications.getqualifications.database;

import java.io.Serializable;

/**
 * 模擬試験の成績を保存する成績情報テーブル(tbl_performance_info)のデータクラス
 */
public class performance_info_data implements Serializable{

	/** 成績ID(P001～P999) */
	private String performance_id;

	/** 回答日時(yyyy-mm-dd hh:mm:ss) */
	private String user_answer_date;

	/** 成績( 0～80 or 0～100 ) */
	private int performance;

	/** 年度 */
	private String year;

	/** 回次 */
	private String turn;

	/**
	 * 成績IDを取得する
	 * @return 成績ID(P001～P999)
	 */
	public String getPerformance_id() {
		return performance_id;
	}

	/**
	 * 成績IDを格納する
	 * @param performance_id 成績ID(P001～P999)
	 */
	public void setPerformance_id(String performance_id) {
		this.performance_id = performance_id;
	}

	/**
	 * 回答日時を取得する
	 * @return 回答日時(yyyy-mm-dd hh:mm:ss)
	 */
	public String getUser_answer_date() {
		return user_answer_date;
	}

	/**
	 * 回答日時を格納する
	 * @param user_answer_date 回答日時(yyyy-mm-dd hh:mm:ss)
	 */
	public void setUser_answer_date(String user_answer_date) {
		this.user_answer_date = user_answer_date;
	}

	/**
	 * 成績を取得する
	 * @return 成績( 0～80 or 0～100 )
	 */
	public int getPerformance() {
		return performance;
	}

	/**
	 * 成績を格納する
	 * @param performance 成績( 0～80 or 0～100 )
	 */
	public void setPerformance(int performance) {
		this.performance = performance;
	}

	/**
	 * 年度を取得する
	 * @return 年度
	 */
	public String getYear() {
		return year;
	}

	/**
	 * 年度を設定する
	 * @param year 年度
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/** 回次を取得する */
	public String getTurn() {
		return turn;
	}

	/**
	 * 回次を設定する
	 * @param turn 回次
	 */
	public void setTurn(String turn) {
		this.turn = turn;
	}
}

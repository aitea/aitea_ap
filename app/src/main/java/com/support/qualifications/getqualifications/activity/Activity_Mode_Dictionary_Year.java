package com.support.qualifications.getqualifications.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.AiteaMode;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.question_info_data;

import java.util.ArrayList;

/**
 * 年度別モードの画面
 */
public class Activity_Mode_Dictionary_Year extends Activity_Base {

    /** モードID */
    private int mode_id;

    /** 年度・回次情報 */
    private question_info_data[] year_turn_data;

    /** 年度・回次情報リストを表示するリストビュー */
    private ListView myListview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary_year);

        //ナビゲーションドロワーセット
        setNavigation((Toolbar) findViewById(R.id.toolbar_mode_dictionary_year));

        //intentからデータ取得
        getIntentValue();

        //全ての年度・回次データを取得
        year_turn_data = getYear_turn_info();

        //リストビューのセット
        setListView(getUsers());

    }
    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * Intentからデータを取得
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
    }

    /**
     * ListViewに表示するデータの定義クラス(User)のArrayListを作成し
     * その結果を返す
     * @return UserのArrayList
     */
    private ArrayList<User> getUsers(){

        ArrayList<User> users = new ArrayList<>();

        //アイコン画像のid
        int icons = 0;

        for (int i = 0; i < year_turn_data.length; i++) {
            User user = new User();
            user.setIcon(BitmapFactory.decodeResource(getResources(), icons));
            user.setYear_turn(year_turn_data[i].getYear() + " " + year_turn_data[i].getTurn());
            users.add(user);
        }
        return users;
    }

    /**
     * ListViewの設定を行う
     * @param users　ListViewに表示するデータ
     */
    private void setListView( ArrayList<User> users ){

        UserAdapter adapter = new UserAdapter(this, 0, users);
        myListview = (ListView)findViewById(R.id.listview_dictionary_year);
        myListview.setEmptyView(findViewById(R.id.empty));
        myListview.setAdapter(adapter);

        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {

                //指定年度・回次の問題IDリストを取得
                String[] q_id_data = getOut_q_id_data(year_turn_data[position].getYear(), year_turn_data[position].getTurn());

                //次の画面のToolbarのタイトルをセット
                String title = year_turn_data[position].getYear() + " " +year_turn_data[position].getTurn();

                //模擬試験モード
                if(mode_id == AiteaMode.TRIALEXAM){
                    //一定時間タップされたビューを非活性(連打防止)
                    setNotEnabledListView();
                    //模擬試験情報テーブルにレコード追加
                    insert_trial_exam_info(q_id_data);
                    //模擬試験問題リスト画面へ遷移
                    goTrialExam_List(q_id_data,title);
                }else{
                    //問題リスト画面へ遷移
                    goDictionary_Question_List(q_id_data,title);
                }
            }
        });
    }

    /**
     * リストビューを非活性にし、一定時間後に活性に戻す
     * リストビュー連打を防止する
     */
    private void setNotEnabledListView(){
        final long time = 3000L;
        myListview.setEnabled(false);
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                myListview.setEnabled(true);
            }
        }, time);
    }

    /**
     * 模擬試験の問題リスト画面に遷移する
     * @param q_id_data 問題IDリスト
     * @param title Toolbarのタイトル
     */
    private void goTrialExam_List(String[] q_id_data,String title){
        Intent intent = new Intent(Activity_Mode_Dictionary_Year.this, Activity_Mode_TrialExam_List.class);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.TOOLBER_TITLE,title);
        startActivity(intent);
        finish();
    }

    /**
     * 問題リスト画面に遷移する
     * 引数は次の画面に渡すデータ
     * @param q_id_data 問題IDリスト
     * @param title Toolbarのタイトル
     */
    private void goDictionary_Question_List(String[] q_id_data,String title){
        Intent intent = new Intent(Activity_Mode_Dictionary_Year.this, Activity_Mode_Dictionary_Question_List.class);
        intent.putExtra(IntentKeyword.MODE_ID,mode_id);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.TOOLBER_TITLE,title);
        startActivity(intent);
    }

    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** アイコン画像 */
        ImageView icon;

        /** 年度と回次( "平成 27年度" )を表示するテキスト */
        TextView year_turn;
    }

    /**
     * ListViewに表示するデータの定義クラス
     */
    public class User{

        /** アイコン画像 */
        private Bitmap icon;

        /** 年度と回次( "平成 27年度" ) */
        private String year_turn;

        /**
         * アイコン画像を格納する
         * @param icon アイコン画像
         */
        public void setIcon(Bitmap icon) {
            this.icon = icon;
        }

        /**
         * 年度と回次を格納する
         * @param year_turn 年度と回次
         */
        public void setYear_turn(String year_turn) {
            this.year_turn = year_turn;
        }

        /**
         * アイコン画像を取得する
         * @return アイコン画像
         */
        public Bitmap getIcon() {return icon;}

        /**
         * 年度と回次を取得する
         * @return 年度と回次( "平成 27年度" )
         */
        public String getYear_turn() {
            return year_turn;
        }

    }

    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.year_list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.yearicon);
                holder.year_turn = (TextView) convertview.findViewById(R.id.year_turn_info);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }
            User user = (User)getItem(pos);
            holder.icon.setImageBitmap(user.getIcon());
            holder.year_turn.setText(user.getYear_turn());
            return convertview;
        }
    }

    /**
     * 全ての年度、回次を降順で取得する
     * @return 全ての年度、回次情報
     */
    private question_info_data[] getYear_turn_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_all_year_turn_info(), null);
        question_info_data[] out_q_i_data = new question_info_data[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new question_info_data();
            out_q_i_data[i].setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            out_q_i_data[i].setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            i++;
        }
        c.close();
        db.close();
        return out_q_i_data;
    }

    /**
     * 引数で指定した年度、回次に属する問題IDを問題IDリストを取得する
     * 該当する問題IDが存在しない場合はnullを返す
     *
     * @param year 年度
     * @param turn　回次
     * @return 問題IDリスト
     */
    private String[] getOut_q_id_data(String year, String turn){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(year, turn), null);
        if(c.getCount() == 0){
            c.close();
            db.close();
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();
        return out_q_i_data;
    }

    /**
     * 模擬試験情報テーブルに該当問題IDリストのレコードを追加する
     * @param q_id_data 問題IDリスト
     */
    private void insert_trial_exam_info(String[] q_id_data){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.beginTransaction();
        for( int i = 0; i < q_id_data.length ; i++ ) {
            db.execSQL(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.add_trial_exam_info(i + 1, q_id_data[i]));
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }


}
package com.support.qualifications.getqualifications.application;

import android.app.Application;

import com.beardedhen.androidbootstrap.TypefaceProvider;

/**
 * Bootstrapを使用する際に必要なクラス
 */
public class Bootstrap extends Application {
    @Override public void onCreate() {
        super.onCreate();
        TypefaceProvider.registerDefaultIconSets();
    }
}

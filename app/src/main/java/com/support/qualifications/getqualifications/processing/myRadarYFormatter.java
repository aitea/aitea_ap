package com.support.qualifications.getqualifications.processing;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * グラフ要素をフォーマットするクラス
 */
public class myRadarYFormatter implements YAxisValueFormatter {

    private DecimalFormat mFormat;

    public myRadarYFormatter() {
        //表示フォーマットを設定
        mFormat = new DecimalFormat("0"); // use one decimal
    }

    @Override
    public String getFormattedValue(float value, YAxis yAxis) {
        return mFormat.format(value) + "%"; // e.g. append a dollar-sign
    }

}

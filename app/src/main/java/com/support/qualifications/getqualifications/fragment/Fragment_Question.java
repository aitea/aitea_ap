package com.support.qualifications.getqualifications.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.question_info_data;
import com.support.qualifications.getqualifications.processing.ImageGetterImpl;

/**
 * 問題文を表示するFragment
 */
public class Fragment_Question extends Fragment {

    /** 作成するView */
    private View QuestinView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        QuestinView = inflater.inflate(R.layout.fragment_question, container, false);

        String q_id = getArguments().getString(IntentKeyword.CURRENT_Q_ID);

        //ユーザの前回の成績をセット
        setUserLastPerformance(q_id);

        //ユーザの正答率をセット
        setUserCorrectRate(q_id);

        //一般の正答率をセット
        //sqlも直す必要あり
//        setGeneralCorrectRate(q_i_data.getQ_correct_rate());

        //問題情報テーブルから必要情報取得
        question_info_data questionInfoData = get_show_data(q_id);

        //ジャンル名をセット
        setGenre_name(questionInfoData);

        //問題文をセット
        setQ_questionsentences(questionInfoData.getQ_sentence(), questionInfoData.getQ_image());

        //選択肢文をセット
        setQ_selectionsentences(questionInfoData);

        return QuestinView;
    }

    /**
     * ジャンル名情報を画面に表示する
     * @param q_i_data ジャンル名を持つ問題情報
     */
    private void setGenre_name(question_info_data q_i_data){
        ((TextView)QuestinView.findViewById(R.id.Q_genre_name)).setText(
                get_genre_name(
                        q_i_data.getField_code(),
                        q_i_data.getL_class_code(),
                        q_i_data.getM_class_code(),
                        q_i_data.getS_class_code()
                )
        );
    }

    /**
     * ユーザの前回の回答成績を画面に表示する
     * @param q_id 問題ID
     */
    private void setUserLastPerformance(String q_id){
        String LastPerformanceText = get_user_last_answer_result(q_id);
        ((TextView)QuestinView.findViewById(R.id.Q_user_last_performance)).setText("　前回の成績： " + LastPerformanceText);
    }

    /**
     * ユーザの正答率を画面に表示する
     * @param q_id 問題ID
     */
    private void setUserCorrectRate(String q_id){
        int[] answer_history = get_user_answer_history(q_id);
        ((TextView)QuestinView.findViewById(R.id.Q_user_correct_rate)).setText("今までの成績： " + answer_history[0] + " / " + answer_history[1]);
    }

    /**
     * 一般正答率を画面に表示する
     * @param correct_reate 一般正答率
     */
    private void setGeneralCorrectRate(double correct_reate){
        //((TextView)QuestinView.findViewById(R.id.Q_general_correct_rate)).setText("　　一般正答率：  " + String.format("%.1f", correct_reate * 100) + "%");
    }

    /**
     * 問題文のビューをセットする
     * @param Q_sentence 問題文に表示する文字列
     * @param Q_image 問題文に表示する画像
     */
    private void setQ_questionsentences(String Q_sentence, Bitmap Q_image){

        if( Q_sentence == null  )
            return;

        // delimiterで区切って配列に格納
        String[] delimited_Q_sentence = Q_sentence.split("\\[\\[\\[img\\]\\]\\]", -1);

        TextView Q_SentenceTextView1 = ((TextView) QuestinView.findViewById(R.id.Q_question_sentence_1));
        TextView Q_SentenceTextView2 = ((TextView) QuestinView.findViewById(R.id.Q_question_sentence_2));

        //Text-Only
        if (delimited_Q_sentence.length == 1 ) {
            Q_SentenceTextView1.setText(getResourceIncludeDrawable((int)Q_SentenceTextView1.getTextSize(), delimited_Q_sentence[0]));
            Q_SentenceTextView2.setVisibility(View.GONE);
        }
        else {
            setQ_SentenceTextView(Q_SentenceTextView1, delimited_Q_sentence[0]);
            setQ_SentenceTextView(Q_SentenceTextView2, delimited_Q_sentence[1]);
            ((ImageView) QuestinView.findViewById(R.id.Q_question_image)).setImageBitmap(Q_image);
        }
    }

    /**
     * HTMLを解析してリソースを取得する
     * @param textsize TextViewのサイズ
     * @param sentence HTMLタグを含む文字列
     * @return 画像が含まれたテキスト
     */
    private Spanned getResourceIncludeDrawable(int textsize, String sentence){
        ImageGetterImpl imageGetter = new ImageGetterImpl(getActivity());
        imageGetter.setHeight(textsize);
        return Html.fromHtml(sentence, imageGetter, null);
    }

    /**
     * 問題文のテキストビューをセットする
     * 問題文が空("")の場合はテキストビューを削除する
     *
     * @param tv テキストビュー
     * @param Q_sentence テキストビューにセットする問題文
     */
    private void setQ_SentenceTextView(TextView tv, String Q_sentence){

        if( Q_sentence.equals("") ){
            tv.setVisibility(View.GONE);
        }else{
            tv.setText(getResourceIncludeDrawable((int) tv.getTextSize(), Q_sentence));
        }
    }

    /**
     * 選択肢の文字もしくは画像をセットする
     * 選択肢の文字がnullの場合はなにもセットしない
     * @param questionInfoData 選択肢情報を持つ問題情報
     */
    private void setQ_selectionsentences(question_info_data questionInfoData){

        try {
            //'ア'の選択肢文字をセット
            setSelection_SentenceTextView(
                    (LinearLayout)QuestinView.findViewById(R.id.Q_selection_a),
                    ((TextView) QuestinView.findViewById(R.id.Q_selection_sentence_a)),
                    questionInfoData.getSelection_sentence_a()
            );
            //'イ'の選択肢文字をセット
            setSelection_SentenceTextView(
                    (LinearLayout)QuestinView.findViewById(R.id.Q_selection_i),
                    ((TextView) QuestinView.findViewById(R.id.Q_selection_sentence_i)),
                    questionInfoData.getSelection_sentence_i()
            );
            //'ウ'の選択肢文字をセット
            setSelection_SentenceTextView(
                    (LinearLayout)QuestinView.findViewById(R.id.Q_selection_u),
                    ((TextView) QuestinView.findViewById(R.id.Q_selection_sentence_u)),
                    questionInfoData.getSelection_sentence_u()
            );
            //'エ'の選択肢文字をセット
            setSelection_SentenceTextView(
                    (LinearLayout) QuestinView.findViewById(R.id.Q_selection_e),
                    ((TextView) QuestinView.findViewById(R.id.Q_selection_sentence_e)),
                    questionInfoData.getSelection_sentence_e()
            );
            //全ての選択肢が画像の場合
            if( isSelection_SentenceHaveImage(questionInfoData) ) {
                ((ImageView) QuestinView.findViewById(R.id.Q_selection_image)).setImageBitmap(questionInfoData.getSelection_image());
            }

        }catch (NullPointerException e) {
            return;
        }
    }

    /**
     * 選択肢が画像のみであるか判定
     * @param questionInfoData 選択肢情報を持つ問題情報
     * @return 画像のみ:true 画像以外がひとつでもある:false
     */
    private boolean isSelection_SentenceHaveImage(question_info_data questionInfoData){

        //画像を表示することをさす文字列
        final String ImageSymbol = "[[[img]]]";

        //全ての選択肢が画像の場合
        if( questionInfoData.getSelection_sentence_a().equals(ImageSymbol) &&
                questionInfoData.getSelection_sentence_i().equals(ImageSymbol) &&
                questionInfoData.getSelection_sentence_u().equals(ImageSymbol) &&
                questionInfoData.getSelection_sentence_e().equals(ImageSymbol)   )
        {
            return true;
        }
        return false;
    }

    /**
     * 選択肢文のViewが画像であれば選択肢の文字のViewを削除し、
     * 画像出なければ引数の文字列をセットする
     * @param layout  選択肢の文字のView
     * @param tv 文字列をセットするテキストビュー
     * @param Selection_sentence テキストビューにセットする文字列
     */
    private void setSelection_SentenceTextView(LinearLayout layout , TextView tv, String Selection_sentence){

        //テキストビューが画像
        if( Selection_sentence.equals("[[[img]]]") ){
            layout.setVisibility(View.GONE);
        }else{
            tv.setText(getResourceIncludeDrawable((int)tv.getTextSize(), Selection_sentence));
        }
    }

    /**
     * 指定問題IDの問題のユーザの回答数[0]と正解数[1]を返す
     * 一度も回答していない場合は両方0としてを返す
     *
     * @param q_id 問題ID
     * @return ユーザの回答履歴
     */
    private int[] get_user_answer_history(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_ANSWER_ARCHIVE.get_correct_mistake(q_id), null);

        int[] answer_history = new int[2];

        int correct_count = 0;
        while (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE)) == 1){
                correct_count++;
            }
        }
        answer_history[0] = correct_count;
        answer_history[1] = c.getCount();

        c.close();
        db.close();
        return answer_history;
    }

    /**
     * ユーザの前回の回答履歴を返す
     * @param q_id
     * @return 正解；○ 不正解：× 一度も解いていない；－
     */
    private String get_user_last_answer_result(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_ANSWER_ARCHIVE.get_user_last_answer_result(q_id), null);

        String text = "－";
        if (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE)) == 1){
                text ="○";
            }else{
                text ="×";
            }
        }

        c.close();
        db.close();
        return text;
    }
    /**
     * 引数で指定した分野コードと大分類コードと中分類コードと小分類コードに属する分野名と大分類名と中分類名と小分類名を取得する
     * @param field_code	分野コード
     * @param l_class_code	大分類コード
     * @param m_class_code	中分類コード
     * @param s_class_code	小分類コード
     * @return 分野名と大分類名と中分類名と小分類名
     */
    private String get_genre_name(int field_code,int l_class_code,int m_class_code,int s_class_code){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_genre_name(field_code, l_class_code, m_class_code, s_class_code), null);
        if (c.moveToNext()) {
            String genre_name =
                    c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_NAME)) + "＞" +
                            c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_NAME)) + "＞" +
                            c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_NAME)) + "＞" +
                            c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_NAME));
            return genre_name;
        };
        c.close();
        db.close();
        return null;
    }

    /**
     * 引数で指定した問題IDに該当する問題回答画面を表示するのに必要なデータを取得する
     * @param q_id 問題ID
     * @return 問題回答画面を表示するのに必要なデータ
     */
    private question_info_data get_show_data(String q_id) {

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();

        question_info_data out_q_i_data;

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_info(q_id), null);
        if(c.moveToNext()){
            out_q_i_data = new question_info_data();
            out_q_i_data.setField_code(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.FIELD_CODE)));
            out_q_i_data.setL_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE)));
            out_q_i_data.setM_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE)));
            out_q_i_data.setS_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.S_CLASS_CODE)));
            out_q_i_data.setQ_sentence(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_SENTENCE)));
            out_q_i_data.setQ_image(c.getBlob(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_IMAGE)));
            out_q_i_data.setSelection_sentence_a(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_A)));
            out_q_i_data.setSelection_sentence_i(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_I)));
            out_q_i_data.setSelection_sentence_u(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_U)));
            out_q_i_data.setSelection_sentence_e(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_E)));
            out_q_i_data.setSelection_image(c.getBlob(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.SELECTION_IMAGE)));
        }else{
            return null;
        }
        c.close();

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        db.close();

        return out_q_i_data;
    }
}

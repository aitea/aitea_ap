package com.support.qualifications.getqualifications.database;
import com.support.qualifications.getqualifications.common.AiteaCommonData;

/**
 *　SQL文を作成するメソッドをまとめたクラス
 */
public final class DB_SqlMaker {

    public DB_SqlMaker() {}

    /**
     * 問題情報テーブルを扱うSQL文を生成
     */
    public static abstract class TBL_QUESTION_INFO {

        /**
         * 引数で指定した問題IDに該当する問題回答画面を表示するのに必要なデータを取得するsql文を返す
         * @param q_id 問題ID
         * @return 作成したsql文
         */
        public  static final String get_q_info(String q_id) {

            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "," +
                    DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE + "," +
                    DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE + "," +
                    DB_ColumnNames.QUESTION_INFO.S_CLASS_CODE + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_SENTENCE + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_IMAGE + "," +
                    DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_A + "," +
                    DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_I + "," +
                    DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_U + "," +
                    DB_ColumnNames.QUESTION_INFO.SELECTION_SENTENCE_E + "," +
                    DB_ColumnNames.QUESTION_INFO.SELECTION_IMAGE +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";

            return sql;
        }


        /**
         * 引数で指定した分野コードに属する問題IDを年度、回次で降順、問題IDの昇順で取得するsql文を返す
         * @param field_code 分野コード
         * @return 作成したsql文
         */
        public final static String get_q_id(int field_code){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                    " ORDER BY " + DB_ColumnNames.QUESTION_INFO.YEAR + " DESC" + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + " DESC" +"," + DB_ColumnNames.QUESTION_INFO.Q_NO;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードに属する問題IDを年度、回次で降順、問題IDの昇順で取得するsql文を返す
         * @param field_code 分野コード
         * @param l_class_code 大分類コード
         * @return 作成したsql文
         */
        public final static String get_q_id(int field_code , int l_class_code){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                    " AND " + DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE + "=" +  l_class_code +
                    " ORDER BY " + DB_ColumnNames.QUESTION_INFO.YEAR + " DESC" + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + " DESC" +"," + DB_ColumnNames.QUESTION_INFO.Q_NO;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードと中分類コードに属する問題IDを年度、回次で降順、問題IDの昇順で取得するsql文を返す
         * @param field_code 分野コード
         * @param l_class_code 大分類コード
         * @param m_class_code 中分類コード
         * @return 作成したsql文
         */
        public final static String get_q_id(int field_code , int l_class_code , int m_class_code){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                    " AND " + DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE + "=" +  l_class_code +
                    " AND " + DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE + "=" +  m_class_code +
                    " ORDER BY " + DB_ColumnNames.QUESTION_INFO.YEAR + " DESC" + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + " DESC" +"," + DB_ColumnNames.QUESTION_INFO.Q_NO;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードと中分類コードと小分類コードに属する問題IDを年度、回次で降順、問題IDの昇順で取得するsql文を返す
         * @param field_code 分野コード
         * @param l_class_code 大分類コード
         * @param m_class_code 中分類コード
         * @param s_class_code 小分類コード
         * @return 作成したsql文
         */
        public final static String get_q_id(int field_code , int l_class_code , int m_class_code , int s_class_code){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                    " AND " + DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE + "=" +  l_class_code +
                    " AND " + DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE + "=" +  m_class_code +
                    " AND " + DB_ColumnNames.QUESTION_INFO.S_CLASS_CODE + "=" +  s_class_code +
                    " ORDER BY " + DB_ColumnNames.QUESTION_INFO.YEAR + " DESC" + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + " DESC" +"," + DB_ColumnNames.QUESTION_INFO.Q_NO;
            return sql;
        }

        /**
         * 引数で指定した年度、回次に属する問題IDを問題IDの昇順で取得するsql文を返す
         * @param year 年度
         * @param turn　回次
         * @return 作成したsql文
         */
        public final static String get_q_id(String year , String turn){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.YEAR + "=" + "\'" + year + "\'"+
                    " AND " + DB_ColumnNames.QUESTION_INFO.TURN + "=" + "\'" + turn + "\'"+
                    " ORDER BY " + DB_ColumnNames.QUESTION_INFO.Q_NO;
            return sql;
        }

        /**
         * 全ての年度、回次を降順で取得するsql文を返す
         * @return 作成したsql文
         */
        public final static String get_all_year_turn_info(){
            String sql = "SELECT " + "DISTINCT " +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " ORDER BY " + DB_ColumnNames.QUESTION_INFO.YEAR + " DESC" + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + " DESC";
            return sql;
        }

        /**
         * 引数で指定した問題IDに該当する、解答画面表示に必要な情報を表示する
         * @param q_id 問題ID
         * @return sql文
         */
        public final static String get_correctAnswer_info(String q_id){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NO + "," +
                    DB_ColumnNames.QUESTION_INFO.CORRECT_ANSWER +
                    " FROM " +  DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 間違い回数をインクリメントする
         * @param q_id	問題ID
         * @return sql文
         */
        public static final String increment_mistake_count(String q_id){
            String sql = "UPDATE " +
                    DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.QUESTION_INFO.MISTAKE_COUNT  +
                    "= (" + DB_ColumnNames.QUESTION_INFO.MISTAKE_COUNT + " + 1 )" +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 間違い回数を全て0にする
         * @return sql文
         */
        public static final String reset_mistake_count(){
            String sql = "UPDATE " +
                    DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.QUESTION_INFO.MISTAKE_COUNT + "=" + 0 ;
            return sql;
        }

        /**
         * 問題情報から問題IDと分野コードと大・中・小分類コードを取得する
         * @return sql文
         */
        public static final String get_all_genre_q_id_info(){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID + "," +
                    DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "," +
                    DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE + "," +
                    DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE + "," +
                    DB_ColumnNames.QUESTION_INFO.S_CLASS_CODE +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG + "!=" + 1;
            return sql;
        }

        /**
         * 問題情報から問題ID・年度・回次・問題番号・問題名を取得する
         * @return sql文
         */
        public static final String get_all_list_q_id_info(){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID + "," +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NO + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NAME +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME;
            return sql;
        }

        /**
         * 復習モードに必要な問題情報リストを間違い回数順に取得する
         * @return sql文
         */
        public static final String get_list_q_i_info_forMistakeCount(int limit){

            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID + "," +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NO + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NAME + "," +
                    DB_ColumnNames.QUESTION_INFO.MISTAKE_COUNT +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG + "!=" + 1 +
                    " AND " + DB_ColumnNames.QUESTION_INFO.MISTAKE_COUNT + ">=" + 1 +
                    " ORDER BY " + DB_ColumnNames.QUESTION_INFO.MISTAKE_COUNT + " DESC" +
                    " LIMIT " + limit;
            return sql;
        }


        /**
         * 問題情報からお気に入りモード表示に必要なを取得する
         * @return sql文
         */
        public static final String get_list_q_i_info_forFavorite(){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID + "," +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NO + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NAME +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG + "=" + 1 ;;
            return sql;
        }
        /**
         * 問題情報から克服一覧表示に必要なを取得する
         * @return sql文
         */
        public static final String get_list_q_i_info_forOvercome(){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_ID + "," +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NO + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NAME +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG + "=" + 1 ;;
            return sql;
        }


        /**
         * 指定問題IDの問題名を取得する
         * @param q_id	問題ID
         * @return sql文
         */
        public  static final String get_q_name(String q_id) {

            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.Q_NAME +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";

            return sql;
        }

        /**
         * 指定問題IDの年度・回次を取得する
         * @param q_id 問題ID
         * @return sql文
         */
        public  static final String get_year_turn(String q_id) {
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";

            return sql;
        }

        /**
         * 指定問題IDの年度・回次・問題番号を取得する
         * @param q_id 問題ID
         * @return sql文
         */
        public  static final String get_year_turn_qno(String q_id) {
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                    DB_ColumnNames.QUESTION_INFO.TURN + "," +
                    DB_ColumnNames.QUESTION_INFO.Q_NO +
                    " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";

            return sql;
        }

        /**
         * 問題情報から指定問題IDの解答を取得する
         * @param q_id	問題ID
         * @return sql文
         */
        public final static String get_correctAnswer(String q_id){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.CORRECT_ANSWER +
                    " FROM " +  DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 問題情報から指定問題IDのお気に入りフラグを取得する
         * @param q_id 問題ID
         * @return sql文
         */
        public final static String get_favorite_flag(String q_id){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG +
                    " FROM " +  DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 問題情報から指定問題IDの克服フラグを取得する
         * @param q_id 問題ID
         * @return sql文
         */
        public final static String get_overcome_flag(String q_id){
            String sql = "SELECT " +
                    DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG +
                    " FROM " +  DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 指定問題IDのお気に入りフラグを設定する
         * @param q_id 問題ID
         * @param favoriteflag 設定値
         * @return sql文
         */
        public static final String set_favorite_flag(String q_id,int favoriteflag){
            String sql = "UPDATE " +
                    DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG  +
                    "= " + favoriteflag +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 指定問題IDの克服フラグを設定する
         * @param q_id 問題ID
         * @param overcome_flag 設定値
         * @return sql文
         */
        public static final String set_overcome_flag(String q_id,int overcome_flag){
            String sql = "UPDATE " +
                    DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG  +
                    "= " + overcome_flag +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 全てのお気に入りフラグをおろす
         * @return sql文
         */
        public static final String down_all_favorite_flag(){
            String sql = "UPDATE " +
                    DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG  +
                    "= " + 0 +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG + "=" + 1;
            return sql;
        }
        /**
         * 全ての克服フラグをおろす
         * @return sql文
         */
        public static final String down_all_overcome_flag(){
            String sql = "UPDATE " +
                    DB_ColumnNames.QUESTION_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG  +
                    "= " + 0 +
                    " WHERE " + DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG + "=" + 1;
            return sql;
        }
    }

    /**
     * ジャンル情報テーブルを扱うSQL文を生成
     */
    public static abstract class TBL_GENRE_INFO {

        /**
         * すべてのジャンルコード情報を取得する
         * @return 作成したsql文
         */
        public static final String get_all_genre_code_info(){
            String sql = "SELECT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_CODE +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME;
            return sql;
        }

        /**
         * すべてのジャンルコードとジャンル名情報を取得する
         * @return 作成したsql文
         */
        public static final String get_all_genre_code_name_info(){
            String sql = "SELECT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.FIELD_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME;
            return sql;
        }

        /**
         * 全ての分野コード、分野名を取得するsql文を返す
         * @return 作成したsql文
         */
        public static final String get_all_field_info(){
            String sql = "SELECT " + "DISTINCT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.FIELD_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME;
            return sql;
        }

        /**
         * 引数で指定した分野コードに属する大分類コード、大分類名を取得するsql文を返す
         * @param field_code 分野コード
         * @return 作成したsql文
         */
        public static final String get_l_class_info(int field_code){
            String sql = "SELECT " + "DISTINCT " +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードに属する中分類コード、中分類名を取得するsql文を返す
         * @param field_code 分野コード
         * @param l_class_code 大分類コード
         * @return 作成したsql文
         */
        public static final String get_m_class_info(int field_code,int l_class_code){
            String sql = "SELECT " + "DISTINCT " +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code +
                    " AND " +  DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードと中分類コードに属する小分類コード、小分類名を取得するsql文を返す
         * @param field_code 分野コード
         * @param l_class_code 大分類コード
         * @param m_class_code 中分類コード
         * @return 作成したsql文
         */
        public static final String get_s_class_info(int field_code,int l_class_code,int m_class_code){
            String sql = "SELECT " + "DISTINCT " +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "=" + m_class_code;
            return sql;
        }

        /**
         * 引数で指定した分野コードに属する分野名を取得する
         * @param field_code 分野コード
         * @return sql文
         */
        public static final String get_genre_name(int field_code){
            String sql = "SELECT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードに属する分野名と大分類名を取得する
         * @param field_code	分野コード
         * @param l_class_code	大分類コード
         * @return sql文
         */
        public static final String get_genre_name(int field_code,int l_class_code){
            String sql = "SELECT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code +
                    " AND " +  DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードと中分類コードに属する分野名と大分類名と中分類名を取得する
         * @param field_code	分野コード
         * @param l_class_code	大分類コード
         * @param m_class_code	中分類コード
         * @return sql文
         */
        public static final String get_genre_name(int field_code,int l_class_code,int m_class_code){
            String sql = "SELECT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "=" + m_class_code;
            return sql;
        }

        /**
         * 引数で指定した分野コードと大分類コードと中分類コードと小分類コードに属する分野名と大分類名と中分類名と小分類名を取得する
         * @param field_code	分野コード
         * @param l_class_code	大分類コード
         * @param m_class_code	中分類コード
         * @param s_class_code	小分類コード
         * @return sql文
         */
        public static final String get_genre_name(int field_code,int l_class_code,int m_class_code,int s_class_code){
            String sql = "SELECT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_NAME + "," +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_NAME +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "=" + m_class_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.S_CLASS_CODE + "=" + s_class_code;
            return sql;
        }


        /**
         * 該当小分類の正答率の更新
         * @param field_code			分野コード
         * @param l_class_code			大分類コード
         * @param m_class_code			中分類コード
         * @param s_class_code			小分類コード
         * @param s_class_molec_denomin	小分類の正答率の分母
         * @param s_class_correct_rate	小分類の正答率の分子
         * @return sql文
         */
        public static final String update_s_class_correct_rate(int field_code,int l_class_code,int m_class_code,int s_class_code,double[] s_class_molec_denomin,double s_class_correct_rate){
            String sql = "UPDATE " +
                    DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.GENRE_INFO.S_CLASS_MOLECULE +
                    "= " + s_class_molec_denomin[0]  +
                    "," + DB_ColumnNames.GENRE_INFO.S_CLASS_DENOMINATOR +
                    "= " + s_class_molec_denomin[1]  +
                    "," + DB_ColumnNames.GENRE_INFO.S_CLASS_CORRECT_RATE  +
                    "= " + s_class_correct_rate  +
                    " WHERE " + DB_ColumnNames.GENRE_INFO.FIELD_CODE + "=" + field_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "=" + m_class_code +
                    " AND " + DB_ColumnNames.GENRE_INFO.S_CLASS_CODE + "=" + s_class_code;
            return sql;
        }


        /**
         * 全ジャンルの正答率情報をリセットする
         * @return sql文
         */
        public static final String reset_each_class_correct_rate(){
            String sql = "UPDATE " +
                    DB_ColumnNames.GENRE_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.GENRE_INFO.S_CLASS_MOLECULE +
                    "= " + 0 +
                    "," + DB_ColumnNames.GENRE_INFO.S_CLASS_DENOMINATOR +
                    "= " + 0 +
                    "," + DB_ColumnNames.GENRE_INFO.S_CLASS_CORRECT_RATE  +
                    "= " + 0  +
                    "," + DB_ColumnNames.GENRE_INFO.M_CLASS_CORRECT_RATE  +
                    "= " + 0  +
                    "," + DB_ColumnNames.GENRE_INFO.L_CLASS_CORRECT_RATE  +
                    "= " + 0  +
                    "," + DB_ColumnNames.GENRE_INFO.FIELD_CORRECT_RATE  +
                    "= " + 0 ;
            return sql;
        }

        /**
         * 分野コード・大・中・小分類コードと小分類の正答率の取得
         * @return sql文
         */
        public static final String get_all_genre_correct_rate(){
            String sql = "SELECT " +
                    DB_ColumnNames.GENRE_INFO.FIELD_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.L_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.M_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_CODE + "," +
                    DB_ColumnNames.GENRE_INFO.S_CLASS_CORRECT_RATE +
                    " FROM " + DB_ColumnNames.GENRE_INFO.TABLE_NAME;
            return sql;
        }
    }

    /**
     * 回答履歴テーブルを扱うSQL文を生成
     */
    public static abstract class TBL_ANSWER_ARCHIVE{

        /**
         * 回答履歴テーブルにレコードを追加する
         * @param user_answer_id	回答ID
         * @param q_id				問題ID
         * @param user_answer		回答
         * @param correct_mistake	正誤
         * @param user_answer_date	回答日
         * @return sql文
         */
        public static final String add_answer_archive_info(String user_answer_id,String q_id,String user_answer,int correct_mistake , String user_answer_date){
            String sql = "INSERT INTO " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME +
                    " VALUES " + "(" +
                    "\'" + user_answer_id + "\'" + "," +
                    "\'" + q_id + "\'" + "," +
                    "\'" + user_answer + "\'" + "," +
                    correct_mistake + "," +
                    "\'" + user_answer_date + "\'" + " )";
            return sql;
        }

        /**
         * 回答履歴情報のレコード数を取得する
         * @return sql文
         */
        public static final String get_user_answer_id_count(){

            String sql = "SELECT " +
                    "count(*) as " + DB_ColumnNames.ANSWER_ARCHIVE_COUNT.ID  +
                    " FROM " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME;

            return sql;
        }

        /**
         * 指定問題IDの回答履歴の正誤をすべて取得する
         * @param q_id 問題ID
         * @return sql文
         */
        public static final String get_correct_mistake(String q_id){

            String sql = "SELECT " +
                    DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE +
                    " FROM " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.ANSWER_ARCHIVE.Q_ID + "=" + "\'" + q_id + "\'";
            return sql;
        }

        /**
         * 指定問題IDの回答履歴の最新の正誤を取得する
         * @param q_id 問題ID
         * @return sql文
         */
        public static final String get_user_last_answer_result(String q_id){

            String sql = "SELECT " +
                    DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE +
                    " FROM " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.ANSWER_ARCHIVE.Q_ID + "=" + "\'" + q_id + "\'" +
                    " ORDER BY " + DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER_ID + " DESC " +
                    " LIMIT " + 1;
            return sql;
        }

    }
    /**
     * サーバ送信情報テーブルを扱うSQL文を生成
     */
    public static abstract class TBL_SERVER_SEND_INFO{

        /**
         * サーバ送信情報テーブルにレコードを追加する
         * @param send_id			送信ID
         * @param user_answer_id	回答ID
         * @param regist_date		登録日時
         * @return sql文
         */
        public static final String add_server_send_info(String send_id,String user_answer_id, String regist_date){
            String sql = "INSERT INTO " + DB_ColumnNames.SERVER_SEND_INFO.TABLE_NAME +
                    " VALUES " + "(" +
                    "\'" + send_id + "\'" + "," +
                    "\'" + user_answer_id + "\'" + "," +
                    "\'" + regist_date + "\'" + " )";
            return sql;
        }


        /**
         * サーバ送信情報のレコード数を取得する
         * @return sql文
         */
        public static final String get_send_id_count(){

            String sql = "SELECT " +
                    "count(*) as " + DB_ColumnNames.SERVER_SEND_INFO_COUNT.ID  +
                    " FROM " + DB_ColumnNames.SERVER_SEND_INFO.TABLE_NAME;

            return sql;
        }

    }

    /**
     * 模擬試験情報テーブルを扱うSQL文を生成
     */
    public static abstract class TBL_TRIAL_EXAM_INFO{

        /**
         * 模擬試験情報から回答を取得する
         * @return sql文
         */
        public static final String get_trial_exam_user_answer(){
            String sql = "SELECT " +
                    DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER +
                    " FROM " + DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME;
            return sql;
        }
        /**
         * 模擬試験情報から指定出題IDの回答を取得する
         * @param set_q_id	出題ID
         * @return sql文
         */
        public final static String get_trial_exam_user_answer(int set_q_id){
            String sql = "SELECT " +
                    DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER +
                    " FROM " +  DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME +
                    " WHERE " + DB_ColumnNames.TRIAL_EXAM_INFO.SET_Q_ID + "=" + set_q_id;
            return sql;
        }

        /**
         * 模擬試験情報テーブルにレコードを追加する
         * @param set_q_id	出題ID
         * @param q_id		    問題ID
         * @return sql文
         */
        public static final String add_trial_exam_info( int set_q_id,String q_id){

            String sql = "INSERT INTO " + DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME +
                    " ( " + DB_ColumnNames.TRIAL_EXAM_INFO.SET_Q_ID + "," +
                    DB_ColumnNames.TRIAL_EXAM_INFO.Q_ID + "," +
                    DB_ColumnNames.TRIAL_EXAM_INFO.ELAPSED_TIME + " )" +
                    " VALUES " + "(" +
                    set_q_id + "," +
                    "\'" + q_id + "\'" +  "," +
                    0 +")";
            return sql;
        }

        /**
         * 指定出題IDの模擬試験情報テーブルの回答・正誤・回答日時・経過時間を更新する
         * @param set_q_id			出題ID
         * @param user_answer		    回答
         * @param correct_mistake	正誤
         * @param user_answer_date	回答日時
         * @param elapsed_time		経過時間
         * @return sql文
         */
        public static final String update_trial_exam_info(int set_q_id,String user_answer,int correct_mistake,String user_answer_date,int elapsed_time){
            String sql = "UPDATE " +
                    DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME +
                    " SET " + DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER +
                    "= " + "\'" + user_answer + "\'" +
                    "," + DB_ColumnNames.TRIAL_EXAM_INFO.CORRECT_MISTAKE +
                    "= " + correct_mistake  +
                    "," + DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER_DATE  +
                    "= "+ "\'" + user_answer_date + "\'" +
                    "," + DB_ColumnNames.TRIAL_EXAM_INFO.ELAPSED_TIME  +
                    "= " + "(" + DB_ColumnNames.TRIAL_EXAM_INFO.ELAPSED_TIME + " + " + elapsed_time  + ")" +
                    " WHERE " + DB_ColumnNames.TRIAL_EXAM_INFO.SET_Q_ID + "=" + set_q_id;
            return sql;
        }

        /**
         * 模擬試験情報テーブルから問題IDを取得する
         * @return sql文
         */
        public final static String get_trial_exam_q_id(){
            String sql = "SELECT " +
                    DB_ColumnNames.TRIAL_EXAM_INFO.Q_ID +
                    " FROM " +  DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME;
            return sql;
        }

        /**
         * 模擬試験情報を取得する
         * 取得した情報は模擬試験結果画面の表示と各種データベースの更新処理
         * に使われる
         *
         * @return sql文
         */
        public final static String get_trial_exam_info(){
            String sql = "SELECT " +
                    DB_ColumnNames.TRIAL_EXAM_INFO.Q_ID +  "," +
                    DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER + "," +
                    DB_ColumnNames.TRIAL_EXAM_INFO.CORRECT_MISTAKE +  "," +
                    DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER_DATE +  "," +
                    DB_ColumnNames.TRIAL_EXAM_INFO.ELAPSED_TIME +
                    " FROM " +  DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME;
            return sql;
        }
    }

    /**
     * 成績情報テーブルを扱うSQL文を生成
     */
    public static abstract class TBL_PERFORMANCE_INFO{

        /**
         * 成績情報にレコードを追加する
         * @param peformance_id		成績ID
         * @param user_answer_date	回答日時
         * @param peformance		成績
         * @param year 年度
         * @param turn 回次
         * @return sql文
         */
        public static final String add_performance_info(String peformance_id,String user_answer_date,int peformance,String year,String turn){
            String sql = "INSERT INTO " + DB_ColumnNames.PERFORMANCE_INFO.TABLE_NAME +
                    " VALUES " + "(" +
                    "\'" + peformance_id + "\'" + "," +
                    "\'" + user_answer_date + "\'" + "," +
                    peformance + "," +
                    "\'" + year + "\'" + "," +
                    "\'" + turn + "\'" + ")";
            return sql;
        }

        /**
         * 成績情報のレコード数を取得する
         * @return sql文
         */
        public static final String get_performance_id_count(){

            String sql = "SELECT " +
                    "count(*) as " + DB_ColumnNames.PERFORMANCE_INFO_COUNT.ID  +
                    " FROM " + DB_ColumnNames.PERFORMANCE_INFO.TABLE_NAME;

            return sql;
        }

        /**
         * 成績と回答日時を取得する
         * なお、取得順は成績IDの降順(つまり新しい順)である
         *
         * @param limit	取得件数
         * @return sql文
         */
        public static final String get_performance(int limit) {
            String sql = "SELECT " +
                    DB_ColumnNames.PERFORMANCE_INFO.PERFORMANCE + "," +
                    DB_ColumnNames.PERFORMANCE_INFO.USER_ANSWER_DATE + "," +
                    DB_ColumnNames.PERFORMANCE_INFO.YEAR + "," +
                    DB_ColumnNames.PERFORMANCE_INFO.TURN +
                    " FROM " + DB_ColumnNames.PERFORMANCE_INFO.TABLE_NAME +
                    " ORDER BY " + DB_ColumnNames.PERFORMANCE_INFO.PERFORMANCE_ID + " DESC" +
                    " LIMIT " + limit;
            return sql;
        }

//        /**
//         * 成績と回答日時を取得する
//         * なお、取得順は成績IDの降順(つまり新しい順)である
//         * @param limit	取得件数
//         * @return sql文
//         */
//        public static final String get_performance(int limit) {
//            String sql = "SELECT " +
//                    DB_ColumnNames.PERFORMANCE_INFO.PERFORMANCE + "," +
//                    DB_ColumnNames.PERFORMANCE_INFO.USER_ANSWER_DATE + "," +
//                    DB_ColumnNames.PERFORMANCE_INFO.YEAR  + "," +
//                    DB_ColumnNames.PERFORMANCE_INFO.TURN + "," +
//                    " FROM " + DB_ColumnNames.PERFORMANCE_INFO.TABLE_NAME +
//                    " ORDER BY " + DB_ColumnNames.PERFORMANCE_INFO.PERFORMANCE_ID + " DESC" +
//                    " LIMIT " + limit;
//            return sql;
//        }
    }

    /**
     * 回答履歴から正誤を取得する
     * @param field_code	分野コード
     * @param l_class_code	大分類コード
     * @param m_class_code	中分類コード
     * @param s_class_code	小分類コード
     * @param limit			取得件数
     * @return sql文
     */
    public static final String get_correct_info(int field_code, int l_class_code, int m_class_code, int s_class_code,int limit){
        String sql = "SELECT " + DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE +
                " FROM " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME  + " as t1" +
                " INNER JOIN " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME + " as t2" +
                " ON " + "t1." + DB_ColumnNames.ANSWER_ARCHIVE.Q_ID + "=" + "t2." + DB_ColumnNames.QUESTION_INFO.Q_ID  +
                " WHERE " + "t2." + DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "=" + field_code +
                " AND " + "t2." + DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE + "=" + l_class_code +
                " AND " + "t2." + DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE + "=" + m_class_code +
                " AND " + "t2." + DB_ColumnNames.QUESTION_INFO.S_CLASS_CODE + "=" + s_class_code +
                " ORDER BY " + DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER_ID + " DESC "+ "LIMIT " + limit;
        return sql;
    }

    /**
     * 回答履歴から正誤を取得する
     * @param field_code	分野コード
     * @param l_class_code	大分類コード
     * @param m_class_code	中分類コード
     * @param s_class_code	小分類コード
     * @return sql文
     */
    public static final String get_correct_info(int field_code, int l_class_code, int m_class_code, int s_class_code){
        String sql = "SELECT " + DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE +
                " FROM " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME  + " as t1" +
                " INNER JOIN " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME + " as t2" +
                " ON " + "t1." + DB_ColumnNames.ANSWER_ARCHIVE.Q_ID + "=" + "t2." + DB_ColumnNames.QUESTION_INFO.Q_ID  +
                " WHERE " + "t2." + DB_ColumnNames.QUESTION_INFO.FIELD_CODE + "=" + field_code +
                " AND " + "t2." + DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE + "=" + l_class_code +
                " AND " + "t2." + DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE + "=" + m_class_code +
                " AND " + "t2." + DB_ColumnNames.QUESTION_INFO.S_CLASS_CODE + "=" + s_class_code +
                " ORDER BY " + DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER_ID + " DESC ";
        return sql;
    }

    /**
     * サーバ送信情報テーブルの全てのデータを取得するsql文を返す
     * @return 作成したsql文
     */
    public static final String get_server_send_info() {
        String sql = "SELECT " +
                "\'" + AiteaCommonData.MY_EXAM_CATEGORY + "\'" + " as exam_category," +
                DB_ColumnNames.ANSWER_ARCHIVE.Q_ID + "," +
                DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER + "," +
                DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE +
                " FROM " + DB_ColumnNames.SERVER_SEND_INFO.TABLE_NAME + " as t1" +
                " INNER JOIN " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME + " as t2" +
                " ON t1." + DB_ColumnNames.SERVER_SEND_INFO.USER_ANSWER_ID + " = t2." + DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER_ID +
                " ORDER BY " + DB_ColumnNames.SERVER_SEND_INFO.SEND_ID + " ASC";

        return sql;
    }

    /**
     * 復習モードに必要な問題情報リストを日付順に取得する
     * @return sql文
     */
    public static final String get_list_q_i_info_forDate(){

        String sql = "SELECT " +
                "t1." + DB_ColumnNames.QUESTION_INFO.Q_ID + "," +
                DB_ColumnNames.QUESTION_INFO.YEAR + "," +
                DB_ColumnNames.QUESTION_INFO.TURN + "," +
                DB_ColumnNames.QUESTION_INFO.Q_NO + "," +
                DB_ColumnNames.QUESTION_INFO.Q_NAME + "," +
                DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER_DATE +
                " FROM " + DB_ColumnNames.QUESTION_INFO.TABLE_NAME + " as t1" +
                " INNER JOIN " + DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME + " as t2" +
                " ON t1." + DB_ColumnNames.QUESTION_INFO.Q_ID + " = t2." + DB_ColumnNames.ANSWER_ARCHIVE.Q_ID +
                " WHERE " + DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG + "!=" + 1 +
                " AND " + DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE + "=" + 0 +
                " ORDER BY " + DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER_ID + " DESC";
        return sql;
    }
}

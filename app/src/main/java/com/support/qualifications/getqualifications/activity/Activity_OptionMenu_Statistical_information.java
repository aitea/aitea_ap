package com.support.qualifications.getqualifications.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.beardedhen.androidbootstrap.BootstrapText;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.performance_info_data;
import com.support.qualifications.getqualifications.processing.myLineFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * 模擬試験の統計データなどを表示する
 */
public class Activity_OptionMenu_Statistical_information extends Activity_Base {

    private LineChart mLineChart;

    /** 参照する成績の数 */
    private final static int NUMBEROFPERFORME = 5;

    /** 合格点 */
    private final static int PASSINGSCORE = 80;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionmenu_statistical_information);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_optionmenu_trialexamresult);
        toolbar.setTitle(getResources().getText(R.string.title_activity_statistical_info));
        toolbar.setTitleTextColor(getResources().getColor(R.color.toolbartextcolor));

        //ナビゲーションドロワーをセット
        setNavigation(toolbar);

        //グラフの設定
        setLineChart();

        //グラフにデータ挿入
        entry();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * グラフの各種設定を行う
     */
    private void setLineChart(){
        mLineChart = (LineChart) findViewById(R.id.bar_chart);

        //タップしても何も起こらなくする
        mLineChart.setTouchEnabled(false);

        //アニメーション設定
        mLineChart.animateY(1000);

        //グラフ右下の説明を消す
        mLineChart.setDescription("");

        //グラフの要素説明のテキストサイズ設定
        mLineChart.getLegend().setTextSize(12f);

        //軸の幅を太くし、色を黒くする
        mLineChart.getXAxis().setAxisLineWidth(2f);
        mLineChart.getXAxis().setAxisLineColor(Color.BLACK);
        mLineChart.getAxisLeft().setAxisLineWidth(2f);
        mLineChart.getAxisLeft().setAxisLineColor(Color.BLACK);

        //右側に出る要素名を表示しない
        mLineChart.getAxisRight().setEnabled(false);

        //x軸の要素名を下にする
        mLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        //x軸の要素名のテキストサイズ
        mLineChart.getXAxis().setTextSize(8f);

        //表示される値の最小値、最大値を設定する
        mLineChart.getAxisLeft().setAxisMaxValue(PASSINGSCORE);
        mLineChart.getAxisLeft().setAxisMinValue(0f);
    }

    /**
     * グラフにセットするデータを作成し、グラフにセットする
     */
    private void entry() {

        //ユーザの成績ラインデータ
        List<Entry> entryList = new ArrayList<>();

        //合格点のラインデータ
        List<Entry> passEntryList = new ArrayList<>();

        //x軸のデータ
        List<String> xVals = new ArrayList<>();

        //成績取得
        performance_info_data[] performance= get_performance();

        //ラベルの文字をセット
        BootstrapLabel label = (BootstrapLabel)findViewById(R.id.statistical_info_label);
        label.setBootstrapText(new BootstrapText.Builder(getApplicationContext()).addText("過去5回の成績").build());


        int i = 0;
        while( i < NUMBEROFPERFORME ) {

            //成績情報が存在する場合
            if( i < performance.length ){
                xVals.add( getMonthAndDay(performance[i].getUser_answer_date()) + " " + performance[i].getYear().substring(2,4) + performance[i].getTurn().substring(0,1) );
                entryList.add(new Entry((float) performance[i].getPerformance(), i));
            }
            //成績情報が存在しない場合は0点として表示
            else{
                xVals.add(" ");
                entryList.add(new Entry((float)0, i));
            }

            //合格ラインを作成
            Double sucsess = PASSINGSCORE * 0.6;
            passEntryList.add(new Entry(sucsess.floatValue(), i));
            i++;
        }

        //データに名前をつける
        LineDataSet lineDataSet = new LineDataSet(entryList, "あなたの成績");
        lineDataSet.setColor(Color.BLUE);
        LineDataSet passDaraset= new LineDataSet(passEntryList,"合格ライン");
        passDaraset.setColor(Color.RED);

        //文字の大きさを変える
        lineDataSet.setValueTextSize(10f);
        passDaraset.setValueTextSize(10f);

        //データ表記の小数点を消す
        lineDataSet.setValueFormatter(new myLineFormatter());
        passDaraset.setDrawValues(false);

        //線の太さ
        lineDataSet.setLineWidth(2f);
        passDaraset.setLineWidth(2f);

        //点を表示しない
        passDaraset.setDrawCircles(false);

        //最終的に入れるデータ
        List<ILineDataSet> sets = new ArrayList<>();
        sets.add(lineDataSet);
        sets.add(passDaraset);
        setGraphData(xVals, sets);

    }

    /**
     * グラフに表示するデータをセットする
     * @param values x軸に表示する文字列
     * @param set 表示するデータセット
     */
    private void setGraphData(List<String> values, List<ILineDataSet> set) {
        LineData data = new LineData(values, set);
        mLineChart.setData(data);
        //グラフを再描画する
        mLineChart.invalidate();
    }

    /**
     * 成績と回答日時を取得する
     * なお、取得順は成績IDの降順(つまり新しい順)である
     *
     * @return 成績と回答日時をもつ成績情報の配列
     */
    private performance_info_data[] get_performance(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_PERFORMANCE_INFO.get_performance(NUMBEROFPERFORME), null);
        performance_info_data[] performances = new performance_info_data[c.getCount()];

        //昇順に詰める
        int i = c.getCount() - 1;
        while (c.moveToNext()) {
            performances[i] = new performance_info_data();
            performances[i].setPerformance(c.getInt(c.getColumnIndex(DB_ColumnNames.PERFORMANCE_INFO.PERFORMANCE)));
            performances[i].setUser_answer_date(c.getString(c.getColumnIndex(DB_ColumnNames.PERFORMANCE_INFO.USER_ANSWER_DATE)));
            performances[i].setYear(c.getString(c.getColumnIndex(DB_ColumnNames.PERFORMANCE_INFO.YEAR)));
            performances[i].setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.PERFORMANCE_INFO.TURN)));
            i--;
        }
        c.close();
        db.close();
        return performances;
    }

    /**
     * 日付情報から月日のみ抽出する
     * @param date 日付データ
     * @return 月日データ
     */
    private String getMonthAndDay(String date) {
        return Integer.valueOf(date.substring(5, 7)) + "/" + Integer.valueOf(date.substring(8, 10));
    }
}
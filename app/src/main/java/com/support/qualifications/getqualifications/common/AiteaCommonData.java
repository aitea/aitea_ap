package com.support.qualifications.getqualifications.common;

import android.provider.BaseColumns;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * アプリ共通データ
 * 基本情報やITパスポートに仕様を変更する場合はこの部分を変更する
 */
public final class AiteaCommonData {

    /** 試験区分略称 */
    public static final String MY_EXAM_CATEGORY = "AP";

    /** 試験名(タイトルや解説検索に用いる) */
    public static final String MY_EXAM_CATEGORY_NAME = "応用情報";

    public AiteaCommonData() {}
}

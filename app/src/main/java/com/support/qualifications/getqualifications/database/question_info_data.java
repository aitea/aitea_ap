package com.support.qualifications.getqualifications.database;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.Serializable;

/**
 * 問題情報テーブル( tbl_question_info )のデータ型クラス
 */
public class question_info_data implements Serializable{

    private String q_id;
    private String year;
    private String turn;
    private int q_no;
    private int field_code;
    private int l_class_code;
    private int m_class_code;
    private int s_class_code;
    private String q_name;
    private String q_sentence;
    private byte q_image[];
    private String selection_sentence_a;
    private String selection_sentence_i;
    private String selection_sentence_u;
    private String selection_sentence_e;
    private byte selection_image[];
    private String correct_answer;
    private double q_correct_rate;
    private int mistake_count;
    private int favorite_flag;
    private int overcome_flag;
    private double selection_rate_a;
    private double selection_rate_i;
    private double selection_rate_u;
    private double selection_rate_e;
    private double selection_rate_q;

    public String getQ_id() {
        return q_id;
    }

    public void setQ_id(String q_id) {
        this.q_id = q_id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public int getQ_no() {
        return q_no;
    }

    public void setQ_no(int q_no) {
        this.q_no = q_no;
    }

    public int getField_code() {
        return field_code;
    }

    public void setField_code(int field_code) {
        this.field_code = field_code;
    }

    public int getL_class_code() {
        return l_class_code;
    }

    public void setL_class_code(int l_class_code) {
        this.l_class_code = l_class_code;
    }

    public int getM_class_code() {
        return m_class_code;
    }

    public void setM_class_code(int m_class_code) {
        this.m_class_code = m_class_code;
    }

    public int getS_class_code() {
        return s_class_code;
    }

    public void setS_class_code(int s_class_code) {
        this.s_class_code = s_class_code;
    }

    public String getQ_name() {
        return q_name;
    }

    public void setQ_name(String q_name) {
        this.q_name = q_name;
    }

    public String getQ_sentence() {
        return q_sentence;
    }

    public void setQ_sentence(String q_sentence) {
        this.q_sentence = q_sentence;
    }

    public Bitmap getQ_image() {
        /** byte型からbmp型に変換 **/
        Bitmap q_image = getBitmap(this.q_image);
        return q_image;
    }

    public void setQ_image(byte q_image_byte[]) {
        this.q_image = q_image_byte;
    }

    public String getSelection_sentence_a() {
        return selection_sentence_a;
    }

    public void setSelection_sentence_a(String selection_sentence_a) {
        this.selection_sentence_a =  selection_sentence_a;
    }

    public String getSelection_sentence_i() {
        return selection_sentence_i;
    }

    public void setSelection_sentence_i(String selection_sentence_i) {
        this.selection_sentence_i =  selection_sentence_i;
    }

    public String getSelection_sentence_u() {
        return selection_sentence_u;
    }

    public void setSelection_sentence_u(String selection_sentence_u) {
        this.selection_sentence_u =  selection_sentence_u;
    }

    public String getSelection_sentence_e() {
        return selection_sentence_e;
    }

    public void setSelection_sentence_e(String selection_sentence_e) {
        this.selection_sentence_e =  selection_sentence_e;
    }

    public Bitmap getSelection_image() {
        /** byte型からbmp型に変換 **/
        Bitmap selection_image = getBitmap(this.selection_image);
        return selection_image;
    }

    public void setSelection_image(byte selection_image_byte[]) {
        this.selection_image = selection_image_byte;
    }

    public String getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    public double getQ_correct_rate() {
        return q_correct_rate;
    }

    public void setQ_correct_rate(double q_correct_rate) {
        this.q_correct_rate = q_correct_rate;
    }

    public int getMistake_count() {
        return mistake_count;
    }

    public void setMistake_count(int mistake_count) {
        this.mistake_count = mistake_count;
    }

    public int getFavorite_flag() {
        return favorite_flag;
    }

    public void setFavorite_flag(int favorite_flag) {
        this.favorite_flag = favorite_flag;
    }

    public int getOvercome_flag() {
        return overcome_flag;
    }

    public void setOvercome_flag(int overcome_flag) {
        this.overcome_flag = overcome_flag;
    }

    public double getSelection_rate_a() {
        return selection_rate_a;
    }

    public void setSelection_rate_a(double selection_rate_a) {
        this.selection_rate_a = selection_rate_a;
    }

    public double getSelection_rate_i(double aDouble) {
        return selection_rate_i;
    }

    public void setSelection_rate_i(double selection_rate_i) {
        this.selection_rate_i = selection_rate_i;
    }

    public double getSelection_rate_u() {
        return selection_rate_u;
    }

    public void setSelection_rate_u(double selection_rate_u) {
        this.selection_rate_u = selection_rate_u;
    }

    public double getSelection_rate_e() {
        return selection_rate_e;
    }

    public void setSelection_rate_e(double selection_rate_e) {
        this.selection_rate_e = selection_rate_e;
    }

    public double getSelection_rate_q() {
        return selection_rate_q;
    }

    public void setSelection_rate_q(double selection_rate_q) {
        this.selection_rate_q = selection_rate_q;
    }

    /** byte型からbmp型に変換 **/
    private Bitmap getBitmap(byte byte_data[]){
        if(byte_data==null){
            return null;
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(byte_data, 0, byte_data.length);
        return bitmap;
    }
}

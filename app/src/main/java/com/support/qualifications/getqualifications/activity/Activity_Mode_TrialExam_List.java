package com.support.qualifications.getqualifications.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapText;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.AiteaMode;
import com.support.qualifications.getqualifications.common.AiteaPreference;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.question_info_data;
import com.support.qualifications.getqualifications.processing.Finishflag;
import com.support.qualifications.getqualifications.processing.ScrollValue;

import java.util.ArrayList;

/**
 * 模擬試験モードの問題リスト画面
 */
public class Activity_Mode_TrialExam_List extends Activity_Base {

    /** 自動採点フラグを保持するためのプリファレンスのキー */
    public static final String SHOWAUTOMARKALERTFLAG = "com.support.qualifications.getqualifications.SHOWAUTOMARKALERTFLAG";

    /** 問題IDリスト */
    private String[] q_id_data;

    /** ActivityのonRestart時にActivityをfinish()するための判定フラグ */
    public static Finishflag Fflag = new Finishflag();

    /** リストビューのスクロール位置 */
    public static ScrollValue scrollValue =  new ScrollValue();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_trial_exam_list);

        //自動スクロール位置初期化
        scrollValue.setScrollposition(0);

        //Restart時にfinish()するフラグをおろす
        Fflag.setFlag(false);

        //自動採点フラグを立てる
        setShowAutoMarkAlertFlag(true);

        //インテントからデータ取得
        getIntentValue();

        //Toolbarをセット
        setToolbar();

        //ひとつも問題を回答していない
        if (isNotExistAnswerRecode()) {
            //問1の回答画面へ
            goFirstQuestion();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Finishフラグが立っていなければListViewをセットする
        if(!Fflag.getFlag()) {
            //ListViewをセット
            setListView(getUsers());
        }

        //自動採点フラグが立っている
        if(getShowAutoMarkAlertFlag()) {
            //全ての回答が終了した
            if (isExistAnswerRecode()) {
                //自動採点を促すアラートダイアログを表示
                AutoMarkAlertshow();
            }
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Finishフラグが立っていたらRestart時に終了する
        if(Fflag.getFlag()){
            finish();
        }
    }

    /**
     * 問1の回答画面に遷移する
     */
    private void goFirstQuestion(){
        Intent intent = new Intent(Activity_Mode_TrialExam_List.this, Activity_Question.class);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, 0);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.TRIALEXAM);
        startActivity(intent);
    }

    /**
     * Intentからデータを取得する
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        q_id_data = intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);
    }


    /**
     * Toolbarをセットする
     */
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_mode_trial_exam_list);
        setNavigation(toolbar);
        question_info_data questionInfoData = get_year_turn(q_id_data[0]);
        ((TextView)findViewById(R.id.trial_exmal_list_title)).setText("模擬試験" + " " + questionInfoData.getYear() + " " + questionInfoData.getTurn());
        toolbar.inflateMenu(R.menu.trialexam_list);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    // "採点"ボタン
                    case R.id.TrialList_mark:
                        ClickMarkAlertshow();
                        break;
                }
                return true;
            }
        });
    }

    /**
     * ListViewに表示するデータの定義クラス(User)のArrayListを作成し
     * その結果を返す
     * @return UserのArrayList
     */
    private ArrayList<User> getUsers(){

        ArrayList<User> users = new ArrayList<>();

        //icon画像のid
        int icons = 0;

        //問題名リスト
        String[] q_names = get_q_names(q_id_data);

        //問題の回答済み(true)か未回答(false)かを判断する配列
        Boolean[] isAlredy_answers = get_trial_alredy_answer();

        for (int i = 0; i < q_id_data.length; i++) {
            User user = new User();
            user.setQ_info((i + 1));
            user.setQ_summary(q_names[i]);
            user.setIsAnswered(isAlredy_answers[i]);
            users.add(user);
        }

        return users;
    }

    /**
     * ListViewの設定を行う
     * @param users　ListViewに表示するデータ
     */
    private void setListView( ArrayList<User> users){

        UserAdapter adapter = new UserAdapter(this, 0, users);
        final ListView myListview = (ListView) findViewById(R.id.listview_mode_trialexam_list);
        myListview.setEmptyView(findViewById(R.id.empty));
        myListview.setAdapter(adapter);
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {
                goSelectQuestionActivity(position);
            }
        });
        myListview.post(new Runnable() {
            @Override
            public void run() {
                //自動スクロール
                myListview.smoothScrollToPositionFromTop(scrollValue.getScrollposition(),0,400);
            }
        });
    }



    /**
     * ListViewで選択した問題の回答画面に画面遷移する処理を行う
     * @param position 選択した問題の場所
     */
    private void goSelectQuestionActivity(int position){
        Intent intent = new Intent(Activity_Mode_TrialExam_List.this, Activity_Question.class);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.TRIALEXAM);
        startActivity(intent);
    }

    /**
     * 試験結果画面に遷移する
     */
    private void  goActivity_Question_Result(){
        Intent intent = new Intent(Activity_Mode_TrialExam_List.this, Activity_Trial_Exam_Result.class);
        startActivity(intent);
        finish();
    }

    /**
     * 全問題の回答画終了した時点で自動採点を行うフラグを
     * プリファレンスに格納する
     * @param bool true:自動採点を行う , false:自動採点しない
     */
    private void setShowAutoMarkAlertFlag(boolean bool){

        SharedPreferences pref = getSharedPreferences(AiteaPreference.PREFNAME, MODE_PRIVATE);
        SharedPreferences.Editor e = pref.edit();
        e.putBoolean(SHOWAUTOMARKALERTFLAG, bool);
        e.commit();
    }

    /**
     * 全問題の回答画終了した時点で自動採点を行うフラグを
     * プリファレンスから取得する
     * @return true:自動採点を行う , false:自動採点しない
     */
    private boolean getShowAutoMarkAlertFlag(){
        SharedPreferences pref = getSharedPreferences(AiteaPreference.PREFNAME, MODE_PRIVATE);
        return pref.getBoolean(SHOWAUTOMARKALERTFLAG, false);
    }

    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            /* 再利用されるビューが無い場合、 */
            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.trial_exam_list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.info = (com.beardedhen.androidbootstrap.BootstrapLabel) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                holder.icon = (ImageView) convertview.findViewById(R.id.questionicon);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }

            User user = (User)getItem(pos);
            holder.info.setBootstrapText(new BootstrapText.Builder(getContext()).addText("問 " + String.format("%02d", user.getQ_info())).build());
            holder.summary.setText(user.getQ_summary());
            //回答済みの場合は画像を表示する
            if(user.getIsAnswered()) {
                holder.icon.setImageResource(R.drawable.check_mark);
            }else{
                holder.icon.setImageResource(0);
            }
            return convertview;
        }
    }

    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** 問題情報( 問1など )を表示するテキスト */
        com.beardedhen.androidbootstrap.BootstrapLabel info;

        /** 問題名を表示するテキスト */
        TextView summary;

        /** アイコン画像 */
        ImageView icon;
    }
    /**
     * ListViewに表示するデータの定義クラス
     */
    public class User{

        /** 問題No */
        private int q_info;

        /** 問題名 */
        private String q_summary;

        /** 既に回答しているか(true:回答済み , false:未回答) */
        private Boolean isAnswered;

        /**
         * 問題Noを格納する
         * @param q_info 問題No
         */
        public void setQ_info(int q_info) {
            this.q_info = q_info;
        }

        /**
         * 問題名を格納する
         * @param q_summary 問題名
         */
        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        /**
         * 回答状況を格納する
         * @param isAnswered true;回答済み , false:未回答
         */
        public void setIsAnswered(Boolean isAnswered) {
            this.isAnswered = isAnswered;
        }

        /**
         * 問題Noを取得する
         * @return 問題No
         */
        public int getQ_info() {
            return q_info;
        }

        /**
         * 問題名を取得する
         * @return 問題名
         */
        public String getQ_summary() {
            return q_summary;
        }

        /**
         * 回答状況を取得する
         * @return true:回答済み , false:未回答
         */
        public Boolean getIsAnswered() {
            return isAnswered;
        }
    }

    /**
     * 全問題の回答画終了した( 最後の問題の回答が終了した )時点で
     * 自動採点を行うかをユーザに問うアラートダイアログを表示する
     * 1度しか出さない
     */
    private void AutoMarkAlertshow(){

        //一度出したら出さないようにフラグをおろす
        setShowAutoMarkAlertFlag(false);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.alert_title_trial_exam_auto_mark));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_trial_exam_auto_mark));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //試験結果画面に遷移する
                goActivity_Question_Result();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 採点ボタンを押下時のアラートダイアログ表示処理
     */
    private void ClickMarkAlertshow(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.alert_title_trial_exam_mark));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_trial_exam_mark));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //採点画面に遷移する
                goActivity_Question_Result();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 模擬試験情報テーブルに全問題のユーザの回答が存在しないかを判定する
     * @return 全ての問題が回答されていない:true . 1件でも回答している:false
     */
    private boolean isNotExistAnswerRecode(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.get_trial_exam_user_answer(), null);
        while (c.moveToNext()) {
            if( c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER)) != null ) {
                c.close();
                db.close();
                return false;
            }
        }
        c.close();
        db.close();
        return true;
    }

    /**
     * 模擬試験情報テーブルに全問題のユーザの回答が存在するかを判定する
     * @return 全ての問題が回答済み:true . 1件でも回答していない:false
     */
    private boolean isExistAnswerRecode(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.get_trial_exam_user_answer(), null);

        while (c.moveToNext()) {
            if( c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER)) == null ) {
                c.close();
                db.close();
                return false;
            }
        }
        c.close();
        db.close();

        return true;
    }

    /**
     * 模擬試験の全ての問題において、ユーザが既に回答しているか、そうでないかを判定し
     * その内容はboolean型配列として返す
     * @return 回答済み:true 未回答;false
     */
    private Boolean[] get_trial_alredy_answer(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.get_trial_exam_user_answer(), null);
        Boolean[] isAlredy_answers = new Boolean[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            isAlredy_answers[i] = new Boolean(true);
            //回答済みの場合
            if( c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER)) == null ) {
                isAlredy_answers[i] = false;
            }
            i++;
        }
        c.close();
        db.close();

        return isAlredy_answers;
    }

    /**
     * 引数の問題IDリストに該当する問題名リストを取得する
     * @param q_id_data 問題IDリスト
     */
    private String[] get_q_names(String[] q_id_data){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        String[] out_q_name = new String[q_id_data.length];
        for( int i = 0; i < q_id_data.length; i++ ){
            Cursor c = null;
            c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_name(q_id_data[i]), null);
            if (c.moveToNext()) {
                out_q_name[i] = new String();
                out_q_name[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NAME));
            }
            c.close();
        }
        db.close();
        return out_q_name;
    }

    /**
     * 指定問題IDの年度・回次を取得する
     * @param q_id 問題ID
     * @return 年度・回次情報
     */
    private question_info_data get_year_turn(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_year_turn(q_id), null);
        question_info_data questionInfoData = new question_info_data();

        if (c.moveToNext()) {
            questionInfoData.setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            questionInfoData.setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            c.close();
            db.close();
            return questionInfoData;
        }
        c.close();
        db.close();
        return null;
    }
}


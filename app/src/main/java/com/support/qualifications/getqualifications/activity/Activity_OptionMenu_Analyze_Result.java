package com.support.qualifications.getqualifications.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapText;
import com.google.android.gms.ads.AdView;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.AiteaMode;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.genre_info_data;
import com.support.qualifications.getqualifications.processing.set_field_correct_rate_infoComparator;
import com.support.qualifications.getqualifications.processing.set_l_class_correct_rate_infoComparator;
import com.support.qualifications.getqualifications.processing.set_m_class_correct_rate_infoComparator;
import com.support.qualifications.getqualifications.processing.set_s_class_correct_rate_infoComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


public class Activity_OptionMenu_Analyze_Result extends Activity_Base implements Runnable{

    /** 正答率を各分類で何件取得するか */
    private final static int CORRECT_RATE_COUNT = 5;

    /** 分類昇順を表すid */
    private final static int FIELD_ASC_ID = 10;

    /** 分類降順を表すid */
    private final static int FIELD_DESC_ID = 20;

    /** 大分類昇順を表すid */
    private final static int L_CLASS_ASC_ID = 30;

    /** 大分類降順を表すid */
    private final static int L_CLASS_DESC_ID = 40;

    /** 中分類昇順を表すid */
    private final static int M_CLASS_ASC_ID = 50;

    /** 中分類降順を表すid */
    private final static int M_CLASS_DESC_ID = 60;

    /** 小分類昇順を表すid */
    private final static int S_CLASS_ASC_ID = 70;

    /** 小分類降順を表すid */
    private final static int S_CLASS_DESC_ID = 80;

    /** 小分類の正答率を含む全ジャンル情報 */
    private ArrayList<genre_info_data> all_Sclass_corret_rate_datas;

    /** 大分類昇順で取得した正答率をもつジャンル情報 */
    private genre_info_data[] l_class_correct_rate_asc;

    /** 大分類降順で取得した正答率をもつジャンル情報 */
    private genre_info_data[] l_class_correct_rate_desc;

    /** 中分類昇順で取得した正答率をもつジャンル情報 */
    private genre_info_data[] m_class_correct_rate_asc;

    /** 中分類降順で取得した正答率をもつジャンル情報 */
    private genre_info_data[] m_class_correct_rate_desc;

    /** 小分類昇順で取得した正答率をもつジャンル情報 */
    private genre_info_data[] s_class_correct_rate_asc;

    /** 小分類降順で取得した正答率をもつジャンル情報 */
    private genre_info_data[] s_class_correct_rate_desc;

    /** 分野昇順の正答率をもつジャンル情報 */
    private genre_info_data[] field_correct_rate_asc;

    /** 分野降順の正答率をもつジャンル情報 */
    private genre_info_data[] field_correct_rate_desc;

    /** 分野の正答率を表示するリストビュー */
    private ListView ListView_field;

    /** 大分類の正答率を表示するリストビュー */
    private ListView ListView_large;

    /** 中分類の正答率を表示するリストビュー */
    private ListView ListView_middle;

    /** 小分類の正答率を表示するリストビュー */
    private ListView ListView_small;

    /** 分野の正答率を表示切り替えするラジオボタングループ */
    private RadioGroup radioGroup_field;

    /** 大分類の正答率を表示切り替えするラジオボタングループ */
    private RadioGroup radioGroup_large;

    /** 中分類の正答率を表示切り替えするラジオボタングループ */
    private RadioGroup radioGroup_middle;

    /** 小分類の正答率を表示切り替えするラジオボタングループ */
    private RadioGroup radioGroup_small;

    /** プログレスダイアログ(ローディング表示のやつ) */
    private ProgressDialog progressDialog;

    /** 非同期処理 */
    private Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionmenu_analyze_result);

        //ナビゲーションドロワーセット
        setNavigation((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_optionmenu_analyze_result));

        //広告セット
        setAdView((AdView) findViewById(R.id.adView_analyzeResult));

        //RadioButtonのセット
        setRadioButton();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // プログレスダイアログを表示する
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        // スレッドを開始する
        thread = new Thread(this);
        thread.start();

    }
    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    @Override
    public void run() {
        //小ジャンルの正答率を取得
        setSclassCorrectRate();

        //正答率情報を取得
        getCorrect_rate();

        //画面表示非同期処理開始
        this.handler.sendEmptyMessage(0);

        //1秒待つ
        try {
            thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // プログレスダイアログを閉じる
        progressDialog.dismiss();
    }

    //画面表示処理(非同期)
    private Handler handler = new Handler() {
        public void handleMessage(Message message) {

            //TableLayoutにDBから取得したデータを設定
            setListViewforRadio();

        }
    };

    /**
     * 正答率を取得し、ジャンル情報配列郡に格納する
     */
    private void getCorrect_rate(){
        field_correct_rate_asc = get_field_correct_rate("ASC");
        field_correct_rate_desc = get_field_correct_rate("DESC");
        l_class_correct_rate_asc = get_l_class_correct_rate(CORRECT_RATE_COUNT, "ASC");
        l_class_correct_rate_desc = get_l_class_correct_rate(CORRECT_RATE_COUNT, "DESC");
        m_class_correct_rate_asc = get_m_class_correct_rate(CORRECT_RATE_COUNT, "ASC");
        m_class_correct_rate_desc = get_m_class_correct_rate(CORRECT_RATE_COUNT, "DESC");
        s_class_correct_rate_asc = get_s_class_correct_rate(CORRECT_RATE_COUNT, "ASC");
        s_class_correct_rate_desc = get_s_class_correct_rate(CORRECT_RATE_COUNT, "DESC");
    }

    /**
     * ラジオボタンの設定を行う
     */
    private void setRadioButton(){

        radioGroup_field = (RadioGroup) findViewById(R.id.radiogroup_field);
        radioGroup_large = (RadioGroup) findViewById(R.id.radiogroup_large);
        radioGroup_middle = (RadioGroup) findViewById(R.id.radiogroup_middle);
        radioGroup_small = (RadioGroup) findViewById(R.id.radiogroup_small);
        radioGroup_field.check(R.id.radiobutton_bad_field);
        radioGroup_large.check(R.id.radiobutton_bad_large);
        radioGroup_middle.check(R.id.radiobutton_bad_middle);
        radioGroup_small.check(R.id.radiobutton_bad_small);

        radioGroup_field.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                switch (radioButton.getId()) {
                    case R.id.radiobutton_bad_field:
                        if (radioButton.isChecked()) {
                            ListView_field.removeAllViewsInLayout();
                            setListView(getUsers(field_correct_rate_asc, FIELD_ASC_ID), ListView_field, FIELD_ASC_ID);
                        }
                        break;
                    case R.id.radiobutton_good_field:
                        if (radioButton.isChecked()) {
                            ListView_field.removeAllViewsInLayout();
                            setListView(getUsers(field_correct_rate_desc, FIELD_DESC_ID), ListView_field, FIELD_DESC_ID);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        radioGroup_large.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                switch (radioButton.getId()) {
                    case R.id.radiobutton_bad_large:
                        if (radioButton.isChecked()) {
                            ListView_large.removeAllViewsInLayout();
                            setListView(getUsers(l_class_correct_rate_asc, L_CLASS_ASC_ID), ListView_large, L_CLASS_ASC_ID);
                        }
                        break;
                    case R.id.radiobutton_good_large:
                        if (radioButton.isChecked()) {
                            ListView_large.removeAllViewsInLayout();
                            setListView(getUsers(l_class_correct_rate_desc, L_CLASS_DESC_ID), ListView_large, L_CLASS_DESC_ID);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        radioGroup_middle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                switch (radioButton.getId()) {
                    case R.id.radiobutton_bad_middle:
                        if (radioButton.isChecked()) {
                            ListView_middle.removeAllViewsInLayout();
                            setListView(getUsers(m_class_correct_rate_asc, M_CLASS_ASC_ID), ListView_middle, M_CLASS_ASC_ID);
                        }
                        break;
                    case R.id.radiobutton_good_middle:
                        if (radioButton.isChecked()) {
                            ListView_middle.removeAllViewsInLayout();
                            setListView(getUsers(m_class_correct_rate_desc, M_CLASS_DESC_ID), ListView_middle, M_CLASS_DESC_ID);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        radioGroup_small.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                switch (radioButton.getId()) {
                    case R.id.radiobutton_bad_small:
                        if (radioButton.isChecked()) {
                            ListView_small.removeAllViewsInLayout();
                            setListView(getUsers(s_class_correct_rate_asc, S_CLASS_ASC_ID), ListView_small, S_CLASS_ASC_ID);
                        }
                        break;
                    case R.id.radiobutton_good_small:
                        if (radioButton.isChecked()) {
                            ListView_small.removeAllViewsInLayout();
                            setListView(getUsers(s_class_correct_rate_desc, S_CLASS_DESC_ID), ListView_small, S_CLASS_DESC_ID);
                        }
                        break;
                    default:
                        break;
                }
            }
        });

    }

    /**
     * ListVIewをセットする
     */
    private void setListViewforRadio(){

        ListView_field = (ListView)findViewById(R.id.listview_mode_analyze_result_field);
        switch (radioGroup_field.getCheckedRadioButtonId()){
            case R.id.radiobutton_bad_field:
                setListView(getUsers(field_correct_rate_asc, FIELD_ASC_ID), ListView_field, FIELD_ASC_ID);
                break;
            case R.id.radiobutton_good_field:
                setListView(getUsers(field_correct_rate_desc, FIELD_DESC_ID), ListView_field, FIELD_DESC_ID);
                break;
            default:
                break;
        }

        ListView_large = (ListView)findViewById(R.id.listview_mode_analyze_result_large);
        switch (radioGroup_large.getCheckedRadioButtonId()){
            case R.id.radiobutton_bad_large:
                setListView(getUsers(l_class_correct_rate_asc, L_CLASS_ASC_ID), ListView_large, L_CLASS_ASC_ID);
                break;
            case R.id.radiobutton_good_large:
                setListView(getUsers(l_class_correct_rate_desc, L_CLASS_DESC_ID), ListView_large, L_CLASS_DESC_ID);
                break;
            default:
                break;
        }

        ListView_middle = (ListView) findViewById(R.id.listview_mode_analyze_result_middle);
        switch (radioGroup_middle.getCheckedRadioButtonId()){
            case R.id.radiobutton_bad_middle:
                setListView(getUsers(m_class_correct_rate_asc, M_CLASS_ASC_ID), ListView_middle, M_CLASS_ASC_ID);
                break;
            case R.id.radiobutton_good_middle:
                setListView(getUsers(m_class_correct_rate_desc, M_CLASS_DESC_ID), ListView_middle, M_CLASS_DESC_ID);
                break;
            default:
                break;
        }
        ListView_small = (ListView) findViewById(R.id.listview_mode_analyze_result_small);
        switch (radioGroup_small.getCheckedRadioButtonId()){
            case R.id.radiobutton_bad_small:
                setListView(getUsers(s_class_correct_rate_asc, S_CLASS_ASC_ID), ListView_small, S_CLASS_ASC_ID);
                break;
            case R.id.radiobutton_good_small:
                setListView(getUsers(s_class_correct_rate_desc, S_CLASS_DESC_ID), ListView_small, S_CLASS_DESC_ID);
                break;
            default:
                break;
        }

    }


    /**
     * ListViewに表示するデータの定義クラス(User)のArrayListを作成し
     * その結果を返す
     * @return UserのArrayList
     */
    private ArrayList<User> getUsers(genre_info_data[] genreInfoDatas , int id){

        ArrayList<User> users = new ArrayList<>();

        for(int i = 0; i < genreInfoDatas.length; i++) {
            User user = new User();
            user.setRank(i + 1);
            user.setGenre_name(getGenreName(genreInfoDatas[i],id));
            user.setCorrect_rate(getCorrectRate(genreInfoDatas[i],id));
            users.add(user);
        }
        return users;
    }

    /**
     * 分類idで判別し、ジャンル名を取得する
     * @param genreInfoData ジャンル情報
     * @param id 分類id
     * @return ジャンル名
     */
    private String getGenreName(genre_info_data genreInfoData,int id){
        switch (id){
            case FIELD_ASC_ID:
            case FIELD_DESC_ID:
                return genreInfoData.getField_name();
            case L_CLASS_ASC_ID:
            case L_CLASS_DESC_ID:
                return genreInfoData.getField_name() + "＞" + genreInfoData.getL_class_name();
            case M_CLASS_ASC_ID:
            case M_CLASS_DESC_ID:
                return genreInfoData.getField_name() + "＞" + genreInfoData.getL_class_name() + "＞" + genreInfoData.getM_class_name();
            case S_CLASS_ASC_ID:
            case S_CLASS_DESC_ID:
            return genreInfoData.getField_name() + "＞" + genreInfoData.getL_class_name() + "＞" + genreInfoData.getM_class_name() + "＞" + genreInfoData.getS_class_name();
            default:
                return null;
        }
    }

    /**
     * 分類idで判別し、正答率を取得する
     * @param genreInfoData ジャンル情報
     * @param id 分類id
     * @return 正答率
     */
    private double getCorrectRate(genre_info_data genreInfoData, int id){
        switch (id){
            case FIELD_ASC_ID:
            case FIELD_DESC_ID:
                return genreInfoData.getField_correct_rate();
            case L_CLASS_ASC_ID:
            case L_CLASS_DESC_ID:
                return genreInfoData.getL_class_correct_rate();
            case M_CLASS_ASC_ID:
            case M_CLASS_DESC_ID:
                return genreInfoData.getM_class_correct_rate();
            case S_CLASS_ASC_ID:
            case S_CLASS_DESC_ID:
                return genreInfoData.getS_class_correct_rate();
            default:
                return 0;
        }
    }

    /**
     * 分類idで判別し、問題IDリストを取得する
     * @param genreInfoData ジャンル情報
     * @param id 分類id
     * @return 問題IDリスト
     */
    private String[] getOut_q_id_data(genre_info_data genreInfoData,int id){
        switch (id){
            case FIELD_ASC_ID:
            case FIELD_DESC_ID:
                return getOut_q_i_data(genreInfoData.getField_code());
            case L_CLASS_ASC_ID:
            case L_CLASS_DESC_ID:
                return getOut_q_i_data(genreInfoData.getField_code(), genreInfoData.getL_class_code());
            case M_CLASS_ASC_ID:
            case M_CLASS_DESC_ID:
                return getOut_q_i_data(genreInfoData.getField_code(), genreInfoData.getL_class_code(), genreInfoData.getM_class_code());
            case S_CLASS_ASC_ID:
            case S_CLASS_DESC_ID:
                return getOut_q_i_data(genreInfoData.getField_code(), genreInfoData.getL_class_code(), genreInfoData.getM_class_code(), genreInfoData.getS_class_code());
            default:
                return null;
        }
    }

    /**
     * 分類idで判別し、LIstViewが空のときに表示するTextViewを取得する
     * @param id 分類id
     * @return LIstViewが空のときに表示するTextView
     */
    private View getEmptyView(int id){
        switch (id){
            case FIELD_ASC_ID:
            case FIELD_DESC_ID:
                return (TextView)findViewById(R.id.listview_mode_analyze_result_empty_field);
            case L_CLASS_ASC_ID:
            case L_CLASS_DESC_ID:
                return (TextView)findViewById(R.id.listview_mode_analyze_result_empty_large);
            case M_CLASS_ASC_ID:
            case M_CLASS_DESC_ID:
                return (TextView)findViewById(R.id.listview_mode_analyze_result_empty_middle);
            case S_CLASS_ASC_ID:
            case S_CLASS_DESC_ID:
                return (TextView)findViewById(R.id.listview_mode_analyze_result_empty_small);
            default:
                return null;
        }
    }

    /**
     * 分類idで判別し、LIstViewにセットするクリックリスナーを取得する
     * @param id 分類id
     * @return クリックリスナー
     */
    private AdapterView.OnItemClickListener getOnItemClickListener(int id){

        switch (id){
            case FIELD_ASC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(field_correct_rate_asc,position,FIELD_ASC_ID);
                    }
                };
            case FIELD_DESC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(field_correct_rate_desc,position,FIELD_DESC_ID);
                    }
                };
            case L_CLASS_ASC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(l_class_correct_rate_asc,position,L_CLASS_ASC_ID);
                    }
                };
            case L_CLASS_DESC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(l_class_correct_rate_desc,position,L_CLASS_DESC_ID);
                    }
                };
            case M_CLASS_ASC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(m_class_correct_rate_asc,position,M_CLASS_ASC_ID);
                    }
                };
            case M_CLASS_DESC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(m_class_correct_rate_desc,position,M_CLASS_DESC_ID);
                    }
                };
            case S_CLASS_ASC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(s_class_correct_rate_asc,position,S_CLASS_ASC_ID);
                    }
                };
            case S_CLASS_DESC_ID:
                return new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(
                            AdapterView<?> parent,
                            View view,
                            int position,
                            long id
                    ) {
                        goSelectQuestionListActivity(s_class_correct_rate_desc,position,S_CLASS_DESC_ID);
                    }
                };
            default:
                return null;
        }
    }

    /**
     * ListViewの設定を行う
     * @param users　ListViewに表示するデータ
     */
    private void setListView( ArrayList<User> users , ListView myListview , int id){
        UserAdapter adapter = new UserAdapter(this, 0, users);
        myListview.setEmptyView(getEmptyView(id));
        myListview.setAdapter(adapter);
        myListview.setOnItemClickListener(getOnItemClickListener(id));
    }

    /**
     * ListViewで選択したジャンルの問題リスト画面に画面遷移する処理を行う
     * @param position 選択したジャンルの場所
     */
    private void goSelectQuestionListActivity(genre_info_data[] genreInfoData,int position , int id){

        Intent intent = new Intent(Activity_OptionMenu_Analyze_Result.this, Activity_Mode_Dictionary_Question_List.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.GENRE);
        intent.putExtra(IntentKeyword.Q_ID_LIST, getOut_q_id_data(genreInfoData[position], id));
        intent.putExtra(IntentKeyword.TOOLBER_TITLE, getGenreName(genreInfoData[position], id));
        startActivity(intent);
    }

    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.analyze_result_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.rank = (com.beardedhen.androidbootstrap.BootstrapLabel) convertview.findViewById(R.id.analyze_result_item_rank);
                holder.genre_name = (TextView) convertview.findViewById(R.id.analyze_result_item_genre_name);
                holder.correct_rate = (TextView) convertview.findViewById(R.id.analyze_result_item_correct_rate);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }
            User user = (User)getItem(pos);
            holder.rank.setBootstrapText(new BootstrapText.Builder(getContext()).addText(user.getRank() + "位").build());
            holder.genre_name.setText(user.getGenre_name());
            holder.correct_rate.setText(getRateText(user.getCorrect_rate()));
            return convertview;

        }
    }
    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** 順位を表示するテキスト */
        com.beardedhen.androidbootstrap.BootstrapLabel rank;

        /** ジャンル名を表示するテキスト */
        TextView genre_name;

        /** 正答率を表示するテキスト */
        TextView correct_rate;
    }

    /**
     * ListViewに表示するデータの定義クラス
     */
    private class User{

        /** 順位 */
        private int rank;

        /** ジャンル名 */
        private String genre_name;

        /** 正答率 */
        private double correct_rate;

        /**
         * 順位を取得する
         * @return 順位
         */
        public int getRank() {
            return rank;
        }

        /**
         * 順位を設定する
         * @param rank 順位
         */
        public void setRank(int rank) {
            this.rank = rank;
        }

        /**
         * ジャンル名を取得する
         * @return ジャンル名
         */
        public String getGenre_name() {
            return genre_name;
        }

        /**
         * ジャンル名を設定する
         * @param genre_name ジャンル名
         */
        public void setGenre_name(String genre_name) {
            this.genre_name = genre_name;
        }

        /**
         * 正答率を取得する
         * @return 正答率
         */
        public double getCorrect_rate() {
            return correct_rate;
        }

        /**
         * 正答率を設定する
         * @param correct_rate 正答率
         */
        public void setCorrect_rate(double correct_rate) {
            this.correct_rate = correct_rate;
        }
    }

    /**
     * 引数で受け取った正答率の分子と分母を元に、
     * 正答率を演算し結果を返す
     * 正答率の分母が0の場合は0を返す
     * @param molec_denomin
     * @return 正答率
     */
    private double get_correct_rate(double[] molec_denomin){

        if( molec_denomin[1] == 0 ){
            return 0.0;
        }else {
            return molec_denomin[0] / molec_denomin[1];
        }
    }
    /**
     * 引数で指定した分野コードに属する問題IDリストを年度、回次で降順、問題IDの昇順で取得する
    * @param field_code 分野コード
    * @return 問題IDリスト
    */
    private String[] getOut_q_i_data(int field_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code), null);

        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();

        return out_q_i_data;
    }
    /**
     * 引数で指定した分野コードと大分類コードに属する問題IDリストを年度、回次で降順、問題IDの昇順で取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @return 問題IDリスト
     */
    private String[] getOut_q_i_data(int field_code, int l_class_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code, l_class_code), null);

        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();

        return out_q_i_data;
    }


    /**
     * 引数で指定した分野コードと大分類コードと中分類コードに属する問題IDリストを年度、回次で降順、問題IDの昇順で取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @return 問題IDリスト
     */
    private String[] getOut_q_i_data(int field_code, int l_class_code , int m_class_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code, l_class_code, m_class_code), null);
        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();
        return out_q_i_data;
    }


    /**
     * 引数で指定した分野コードと大分類コードと中分類コードと小分類コードに属する問題IDリストをを年度、回次で降順、問題IDの昇順で取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @param s_class_code 小分類コード
     * @return 問題IDリスト
     */
    private String[] getOut_q_i_data(int field_code, int l_class_code , int m_class_code , int s_class_code) {

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code, l_class_code, m_class_code, s_class_code), null);
        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();
        return out_q_i_data;
    }

    /**
     * 正答率を小数点以下1桁で丸めて%表示で返す
     * @param rate 正答率
     * @return 正答率を%表示した文字列
     */
    private String getRateText(double rate){
        return String.format("%.1f",rate * 100) + "%";
    }

    /**
     * 該当する小分類ジャンルの分野コード、分野名、大分類コード、大分類名、中分類コード、中分類名、中分類、小分類名、小分類の正答率を取得する
     * @param limit 取得件数
     * @param order 昇順or降順
     * @return 小分類ジャンルの分野コード、分野名、大分類コード、大分類名、中分類コード、中分類名、中分類、小分類名、小分類の正答率をもつジャンル情報
     */
    private genre_info_data[] get_s_class_correct_rate(int limit, String order){

        genre_info_data[] out_g_i_data = new genre_info_data[limit];

        ArrayList<genre_info_data> sclass_arrayList = (ArrayList<genre_info_data>)all_Sclass_corret_rate_datas.clone();

        if(order.equals("ASC")) {
            Collections.sort(sclass_arrayList, new set_s_class_correct_rate_infoComparator(1));
        }else if (order.equals("DESC")){
            Collections.sort(sclass_arrayList, new set_s_class_correct_rate_infoComparator(-1));
        }else{
            return null;
        }

        for( int i = 0; i < out_g_i_data.length; i++ ){
            out_g_i_data[i] = sclass_arrayList.get(i);
        }
        return out_g_i_data;
    }

    /**
     * 該当する中分類ジャンルの分野コード、分野名、大分類コード、大分類名、中分類コード、中分類名、中分類の正答率を取得する
     * @param limit 取得件数
     * @param order 昇順or降順
     * @return 中分類ジャンルの分野コード、分野名、大分類コード、大分類名、中分類コード、中分類名、中分類の正答率をもつジャンル情報
     */
    private genre_info_data[] get_m_class_correct_rate(int limit, String order){

        genre_info_data[] out_g_i_data = new genre_info_data[limit];

        //小分類正答率を含む全ジャンル情報
        ArrayList<genre_info_data> sclass_arrayList = (ArrayList<genre_info_data>)all_Sclass_corret_rate_datas.clone();

        //中分類のマップ
        HashMap<String,ArrayList<genre_info_data>> mclass_map = new HashMap();

        //マップ作成
        for(int i = 0; i < sclass_arrayList.size(); i++){
            // keyをつくる
            String key = sclass_arrayList.get(i).getField_code() + "_" +
                    sclass_arrayList.get(i).getL_class_code() + "_" +
                    sclass_arrayList.get(i).getM_class_code() + "_" ;

            // 初めて追加されるキー
            if(mclass_map.get(key) == null){
                // マップ追加
                ArrayList<genre_info_data> list = new ArrayList<>();
                list.add(sclass_arrayList.get(i));
                mclass_map.put(key, list);
            } else {
                // 既にあるキー
                mclass_map.get(key).add(sclass_arrayList.get(i));
            }
        }

        //正答率を含む中分類情報のリスト
        ArrayList<genre_info_data> mclass_arrayList = new ArrayList<>();

        //ジャンルコード、ジャンル名と正答率をリストに格納
        for (String key : mclass_map.keySet()) {
            genre_info_data genreInfoData = new genre_info_data();
            genreInfoData.setField_code(mclass_map.get(key).get(0).getField_code());
            genreInfoData.setField_name(mclass_map.get(key).get(0).getField_name());
            genreInfoData.setL_class_code(mclass_map.get(key).get(0).getL_class_code());
            genreInfoData.setL_class_name(mclass_map.get(key).get(0).getL_class_name());
            genreInfoData.setM_class_code(mclass_map.get(key).get(0).getM_class_code());
            genreInfoData.setM_class_name(mclass_map.get(key).get(0).getM_class_name());

            double[] molec_denomin = new double[2];
            molec_denomin[0] = 0;
            molec_denomin[1] = 0;
            for( int i = 0; i < mclass_map.get(key).size(); i++ ){
                molec_denomin[0] += mclass_map.get(key).get(i).getS_class_molecule();
                molec_denomin[1] += mclass_map.get(key).get(i).getS_class_denominator();
            }
            //中分類の正答率をセット
            genreInfoData.setM_class_correct_rate(get_correct_rate(molec_denomin));
            mclass_arrayList.add(genreInfoData);
        }

        if(order.equals("ASC")) {
            Collections.sort(mclass_arrayList, new set_m_class_correct_rate_infoComparator(1));
        }else if (order.equals("DESC")){
            Collections.sort(mclass_arrayList, new set_m_class_correct_rate_infoComparator(-1));
        }else{
            return null;
        }

        for( int i = 0; i < out_g_i_data.length; i++ ){
            out_g_i_data[i] = mclass_arrayList.get(i);
        }
        return out_g_i_data;
    }

    /**
     * 該当する大分類ジャンルの分野コード、分野名、大分類コード、大分類名、大分類の正答率を取得する
     * @param limit 取得件数
     * @param order 昇順or降順
     * @return 大分類ジャンルの分野コード、分野名、大分類コード、大分類名、大分類の正答率をもつジャンル情報
     */
    private genre_info_data[] get_l_class_correct_rate(int limit, String order){

        genre_info_data[] out_g_i_data = new genre_info_data[limit];

        //小分類正答率を含む全ジャンル情報
        ArrayList<genre_info_data> sclass_arrayList = (ArrayList<genre_info_data>)all_Sclass_corret_rate_datas.clone();

        //大分類のマップ
        HashMap<String,ArrayList<genre_info_data>> lclass_map = new HashMap();

        //マップ作成
        for(int i = 0; i < sclass_arrayList.size(); i++){
            // keyをつくる
            String key = sclass_arrayList.get(i).getField_code() + "_" +
                    sclass_arrayList.get(i).getL_class_code() + "_" ;

            // 初めて追加されるキー
            if(lclass_map.get(key) == null){
                // マップ追加
                ArrayList<genre_info_data> list = new ArrayList<>();
                list.add(sclass_arrayList.get(i));
                lclass_map.put(key, list);
            } else {
                // 既にあるキー
                lclass_map.get(key).add(sclass_arrayList.get(i));
            }
        }

        //正答率を含む大分類情報のリスト
        ArrayList<genre_info_data> lclass_arrayList = new ArrayList<>();

        //ジャンルコード、ジャンル名と正答率をリストに格納
        for (String key : lclass_map.keySet()) {
            genre_info_data genreInfoData = new genre_info_data();
            genreInfoData.setField_code(lclass_map.get(key).get(0).getField_code());
            genreInfoData.setField_name(lclass_map.get(key).get(0).getField_name());
            genreInfoData.setL_class_code(lclass_map.get(key).get(0).getL_class_code());
            genreInfoData.setL_class_name(lclass_map.get(key).get(0).getL_class_name());

            double[] molec_denomin = new double[2];
            molec_denomin[0] = 0;
            molec_denomin[1] = 0;
            for (int i = 0; i < lclass_map.get(key).size(); i++) {
                molec_denomin[0] += lclass_map.get(key).get(i).getS_class_molecule();
                molec_denomin[1] += lclass_map.get(key).get(i).getS_class_denominator();
            }

            //大分類の正答率をセット
            genreInfoData.setL_class_correct_rate(get_correct_rate(molec_denomin));
            lclass_arrayList.add(genreInfoData);
        }

        if(order.equals("ASC")) {
            Collections.sort(lclass_arrayList, new set_l_class_correct_rate_infoComparator(1));
        }else if (order.equals("DESC")){
            Collections.sort(lclass_arrayList, new set_l_class_correct_rate_infoComparator(-1));
        }else{
            return null;
        }

        for( int i = 0; i < out_g_i_data.length; i++ ){
            out_g_i_data[i] = lclass_arrayList.get(i);
        }
        return out_g_i_data;
    }

    /**
     * 全分野の分野コード、分野名、正答率を取得する
     * @return  全分野の分野コード、分野名、正答率をを持つジャンル情報
     */
    private genre_info_data[] get_field_correct_rate(String order){

        //小分類正答率を含む全ジャンル情報
        ArrayList<genre_info_data> sclass_arrayList = (ArrayList<genre_info_data>)all_Sclass_corret_rate_datas.clone();

        //分野のマップ
        HashMap<String,ArrayList<genre_info_data>> field_map = new HashMap();

        //マップ作成
        for(int i = 0; i < sclass_arrayList.size(); i++){
            // keyをつくる
            String key = String.valueOf(sclass_arrayList.get(i).getField_code());

            // 初めて追加されるキー
            if(field_map.get(key) == null){
                // マップ追加
                ArrayList<genre_info_data> list = new ArrayList<>();
                list.add(sclass_arrayList.get(i));
                field_map.put(key, list);
            } else {
                // 既にあるキー
                field_map.get(key).add(sclass_arrayList.get(i));
            }
        }

        //正答率を含む大分類情報のリスト
        ArrayList<genre_info_data> field_arrayList = new ArrayList<>();

        //ジャンルコード、ジャンル名と正答率をリストに格納
        for (String key : field_map.keySet()) {
            genre_info_data genreInfoData = new genre_info_data();
            genreInfoData.setField_code(field_map.get(key).get(0).getField_code());
            genreInfoData.setField_name(field_map.get(key).get(0).getField_name());

            double[] molec_denomin = new double[2];
            molec_denomin[0] = 0;
            molec_denomin[1] = 0;
            for (int i = 0; i < field_map.get(key).size(); i++) {
                molec_denomin[0] += field_map.get(key).get(i).getS_class_molecule();
                molec_denomin[1] += field_map.get(key).get(i).getS_class_denominator();
            }

            //分野の正答率をセット
            genreInfoData.setField_correct_rate(get_correct_rate(molec_denomin));
            field_arrayList.add(genreInfoData);
        }

        if(order.equals("ASC")) {
            Collections.sort(field_arrayList, new set_field_correct_rate_infoComparator(1));
        }else if (order.equals("DESC")){
            Collections.sort(field_arrayList, new set_field_correct_rate_infoComparator(-1));
        }else{
            return null;
        }

        genre_info_data[] out_g_i_data = new genre_info_data[field_arrayList.size()];

        for( int i = 0; i < out_g_i_data.length; i++ ){
            out_g_i_data[i] = field_arrayList.get(i);
        }
        return out_g_i_data;
    }
    /**
     * 小ジャンルの正答率を取得する
     */
    private void setSclassCorrectRate(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        all_Sclass_corret_rate_datas = new ArrayList<>();

        db.beginTransaction();
        Cursor c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_all_genre_code_name_info(), null);
        while (c.moveToNext()) {
            genre_info_data genre_code = new genre_info_data();
            genre_code.setField_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_CODE)));
            genre_code.setField_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_NAME)));
            genre_code.setL_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_CODE)));
            genre_code.setL_class_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_NAME)));
            genre_code.setM_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_CODE)));
            genre_code.setM_class_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_NAME)));
            genre_code.setS_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_CODE)));
            genre_code.setS_class_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_NAME)));
            all_Sclass_corret_rate_datas.add(genre_code);
        }
        for(int i = 0; i < all_Sclass_corret_rate_datas.size() ; i++ ) {
            c = db.rawQuery(DB_SqlMaker.get_correct_info(
                    all_Sclass_corret_rate_datas.get(i).getField_code(),
                    all_Sclass_corret_rate_datas.get(i).getL_class_code(),
                    all_Sclass_corret_rate_datas.get(i).getM_class_code(),
                    all_Sclass_corret_rate_datas.get(i).getS_class_code()), null);

            int count = 0;
            int correctCount = 0;
            while (c.moveToNext()) {
                correctCount += c.getInt(c.getColumnIndex(DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE));
                count++;
            }
            double[] molec_denomin = new double[2];
            molec_denomin[0] = correctCount;
            molec_denomin[1] = count;

            all_Sclass_corret_rate_datas.get(i).setS_class_molecule(molec_denomin[0]);
            all_Sclass_corret_rate_datas.get(i).setS_class_denominator(molec_denomin[1]);
            all_Sclass_corret_rate_datas.get(i).setS_class_correct_rate(get_correct_rate(molec_denomin));
        }
        c.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }
}

package com.support.qualifications.getqualifications.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.AiteaMode;
import com.support.qualifications.getqualifications.common.Date;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.question_info_data;
import com.support.qualifications.getqualifications.database.trial_exam_info_data;

import java.util.ArrayList;

/**
 * 試験結果画面
 */
public class Activity_Trial_Exam_Result extends Activity_Base implements Runnable {

    /** 模擬試験情報 */
    private trial_exam_info_data[] trialExamInfoDatas;

    /** ListViewに表示するデータのリスト */
    private ArrayList<User> users = new ArrayList<>();

    /** プログレスダイアログ(ローディング表示のやつ) */
    private ProgressDialog progressDialog;

    /** 非同期処理 */
    private Thread thread;

    /** ユーザの回答した問題数 */
    private int Answer_question_Count;

    /** 正解数 */
    private int correct_count;

    /** 経過時間合計 */
    private int sumElapsedtime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trial_exam_result);

        //Activity_Mode_TrialExam_ListがRestart時にfinish()するフラグを立てる
        Activity_Mode_TrialExam_List.Fflag.setFlag(true);

        //広告セット
        setAdView((AdView) findViewById(R.id.adView_trialExamResult));

        //Toolberセット
        setToolbar();

        // プログレスダイアログを表示する
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.process_dialog_message));
        progressDialog.show();

        // スレッドを開始する
        thread = new Thread(this);
        thread.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * スレッド開始後の処理
     */
    @Override
    public void run() {

        //模擬試験情報テーブルからデータ取得
        trialExamInfoDatas = get_trial_exam_info();

        //模擬試験情報テーブルのレコードを削除する
        delete_tbl_info(DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME);

        //模擬試験結果を元に回答履歴テーブルとサーバ送信情報テーブルにレコードを追加する
        add_answer_archive_and_send_info(trialExamInfoDatas);

        //回答した問題の間違い回数をインクリメントする
        increment_mistake_count(trialExamInfoDatas);

        //模擬試験で回答した問題のデータを抽出し、成績情報テーブルにデータをセットする
        setResultValueToViewsAndDatebase();

        //画面表示非同期処理開始
        this.handler.sendEmptyMessage(0);

        //1秒待つ
        try {
            thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // プログレスダイアログを閉じる
        progressDialog.dismiss();

    }


    //画面表示処理(非同期)
    private Handler handler = new Handler() {
        public void handleMessage(Message message) {

            //合否、正答数、正答率、経過時間をTextViewにセット
            setTextViews(Answer_question_Count, correct_count, sumElapsedtime);

            //ListViewセット
            setListView();

            //スクロールViewセット
            setScrollView();
        }
    };
    /**
     * ScrollViewの設定を行う
     */
    private void setScrollView(){
        final ScrollView scrollView = (ScrollView) findViewById(R.id.result_scrollview);

        //表示後一番上にスクロールする
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(scrollView.FOCUS_UP);
            }
        });
    }

    /**
     * ListViewの設定を行う
     */
    private void setListView(){

        ListView myListview = (ListView)findViewById(R.id.listview_trial_exam_result);

        UserAdapter adapter = new UserAdapter(this, 0, users);

        myListview.setAdapter(adapter);

        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view,
                    int position,
                    long id
            ) {
                //問題解説画面に遷移
                goSelectCorrectAnswerActivity(position);
            }
        });
    }

    /**
     * ListViewで選択した問題の解説画面に画面遷移する処理を行う
     * @param position 選択した問題の場所
     */
    private void goSelectCorrectAnswerActivity(int position){

        Intent intent = new Intent(Activity_Trial_Exam_Result.this, Activity_Question_CorrectAnswer.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.TRIALEXAM);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
        intent.putExtra(IntentKeyword.Q_ID_LIST, trial_exam_info_data_convert_q_id(trialExamInfoDatas));
        intent.putExtra(IntentKeyword.SELECT_ANSWER_LIST, trial_exam_info_data_convert_select_answer(trialExamInfoDatas));
        startActivity(intent);

    }

    /**
     * 模擬試験で回答した問題のデータを抽出し、Viewと成績情報テーブルにデータをセットする
     */
    private void setResultValueToViewsAndDatebase(){

        //模擬試験でといた問題の問題名リストを取得
        String[] q_names = get_q_names(trialExamInfoDatas);

        Answer_question_Count = 0;
        correct_count = 0;
        sumElapsedtime = 0;

        for(int i = 0; i < trialExamInfoDatas.length; i++){

            //回答している問題である
            if( trialExamInfoDatas[i].getUser_answer() != null ) {

                sumElapsedtime += trialExamInfoDatas[i].getElapsed_time();
                Answer_question_Count++;

                //正解の場合
                if (trialExamInfoDatas[i].getCorrect_mistake() == 1) {
                    setUser(BitmapFactory.decodeResource(getResources(), R.drawable.icon_maru), i + 1, q_names[i]);
                    correct_count++;
                } else {
                    setUser(BitmapFactory.decodeResource(getResources(), R.drawable.icon_batsu), i + 1, q_names[i]);
                }
            }
        }

        if(Answer_question_Count == trialExamInfoDatas.length) {

            //年度回次取得
            question_info_data q_info_data = get_year_turn(trialExamInfoDatas[0].getQ_id());

            //取得したデータを成績情報テーブルに追加
            addRecodeToPerformance_info(correct_count,q_info_data.getYear() , q_info_data.getTurn());
        }
    }

    /**
     * TextViewにデータをセットする
     * @param Answer_question_Count 回答数
     * @param correct_count 正答数
     * @param sumElapsedtime 経過時間
     */
    private void setTextViews(int Answer_question_Count, int correct_count, int sumElapsedtime){

        setTextOfResult_passOrfall(Answer_question_Count, correct_count);
        setTextOfCorrext_rate(Answer_question_Count, correct_count);
        ((TextView)findViewById(R.id.result_correct_count)).setText(correct_count + "/" + Answer_question_Count);
        ((TextView)findViewById(R.id.result_elapsed_time)).setText((sumElapsedtime / 60) + "分");
    }

    /**
     * ListViewに表示するデータ(Users)に新規データをセットする
     * @param icon アイコン画像
     * @param q_no 問題No
     * @param q_name 問題名
     */
    private void setUser(Bitmap icon, int q_no, String q_name){
        User user = new User();
        user.setIcon(icon);
        user.setQ_info(q_no);
        user.setQ_summary(q_name);
        users.add(user);
    }

    /**
     * 成績情報テーブルに必要な情報を作成しレコードを追加する
     * @param correct_count 正答数
     * @param year 年度
     * @param turn 回次
     */
    private void addRecodeToPerformance_info(int correct_count,String year , String turn){
        //新規成績IDの取得
        String next_performance_id = "P" +  String.format("%03d", get_performance_id_count() + 1);

        add_performance_info(next_performance_id, Date.getNowDate(), correct_count , year , turn);
    }

    /**
     * 正答率を表示するTextViewを設定する
     * @param Answer_question_Count 回答した問題数
     * @param correct_count  正答数
     */
    private void setTextOfCorrext_rate(int Answer_question_Count , int correct_count){

        //正答率
        float correct_rate;

        //0除算防止
        if(Answer_question_Count == 0){
            correct_rate = (float)correct_count / 1 * 100;
        }else {
            correct_rate = (float) correct_count / Answer_question_Count * 100;
        }

        ((TextView)findViewById(R.id.result_correct_rate)).setText(String.format("%.1f", correct_rate) + "%");
    }

    /**
     * 試験結果を表示するTextViewを設定する
     * @param Answer_question_Count 回答数
     * @param correct_count 正答数
     */
    private void setTextOfResult_passOrfall(int Answer_question_Count,int correct_count){

       TextView textView = (TextView)findViewById(R.id.result_passOrfall);

        //全問回答している
        if( Answer_question_Count == trialExamInfoDatas.length ){
            if( correct_count / (float)trialExamInfoDatas.length >= 0.6 ){
                textView.setText("合格");
                textView.setTextColor(getResources().getColor(R.color.red));
                textView.setBackground(getResources().getDrawable(R.drawable.shape_rouded_corners_good));
            } else {
                textView.setText("不合格");
                textView.setTextColor(getResources().getColor(R.color.blue));
                textView.setBackground(getResources().getDrawable(R.drawable.shape_rouded_corners_bad));
            }
        }else{
            textView.setText("中断");
            textView.setTextColor(getResources().getColor(R.color.DarkOrange));
            textView.setBackground(getResources().getDrawable(R.drawable.shape_rouded_corners_retire));
        }


    }


    /**
     * ツールバーの設定を行う
     */
    private void setToolbar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_trial_exam_result);
        setNavigation(toolbar);
        toolbar.inflateMenu(R.menu.question_result);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    // メニュー内の"ホーム"押下後の処理
                    case R.id.menu_home:
                        finish();
                        break;
                }
                return true;
            }
        });
    }

    /**
     * 模擬試験情報から回答している問題の問題IDを取得する
     * @param trialExamInfoDatas 模擬試験情報
     * @return 問題IDのリスト
     */
    private String[] trial_exam_info_data_convert_q_id(trial_exam_info_data[] trialExamInfoDatas){

        ArrayList<String> q_id_list = new ArrayList<>();

        for( int i = 0; i < trialExamInfoDatas.length; i++ ){

            //ユーザが回答している場合
            if( trialExamInfoDatas[i].getUser_answer() != null ) {
                q_id_list.add(trialExamInfoDatas[i].getQ_id());
            }
        }

        //ArrayList → 配列
        String[] q_id_data = (String[])q_id_list.toArray(new String[q_id_list.size()]);

        return q_id_data;

    }

    /**
     * 模擬試験情報からユーザの回答履歴を取得する
     * @param trialExamInfoDatas 模擬試験情報
     * @return ユーザの回答履歴
     */
    private String[] trial_exam_info_data_convert_select_answer(trial_exam_info_data[] trialExamInfoDatas) {

        ArrayList<String> select_answer_list = new ArrayList<>();

        for (int i = 0; i < trialExamInfoDatas.length; i++) {

            //ユーザが回答している場合
            if (trialExamInfoDatas[i].getUser_answer() != null) {
                select_answer_list.add(trialExamInfoDatas[i].getUser_answer());
            }
        }

        //ArrayList → 配列
        String[] select_answer_data = (String[]) select_answer_list.toArray(new String[select_answer_list.size()]);

        return select_answer_data;
    }

    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            /* 再利用されるビューが無い場合、 */
            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.trial_exam_list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.questionicon);
                holder.info = (TextView) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }

            User user = (User)getItem(pos);
            holder.icon.setImageBitmap(user.getIcon());
            holder.info.setText("問" + String.format("%02d", user.getQ_info()));
            holder.summary.setText(user.getQ_summary());

            return convertview;
        }
    }


    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** アイコン画像 */
        ImageView icon;

        /** 問題情報( 問1など )を表示するテキスト */
        TextView info;

        /** 問題名を表示するテキスト */
        TextView summary;
    }

    /**
     * ListViewに表示するデータの定義クラス
     */
    public class User{

        /** アイコン画像 */
        private Bitmap icon;

        /** 問題情報( 問1など ) */
        private int q_info;

        /** 問題名 */
        private String q_summary;
        /**
         * アイコン画像を格納する
         * @param icon アイコン画像
         */
        public void setIcon(Bitmap icon) {
            this.icon = icon;
        }

        /**
         * 問題情報を格納する
         * @param q_info 問題情報( 問1 平成**年度 *期 )
         */
        public void setQ_info(int q_info) {
            this.q_info = q_info;
        }

        /**
         * 問題名を格納する
         * @param q_summary 問題名
         */
        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        /**
         * アイコン画像を取得する
         * @return アイコン画像
         */
        public Bitmap getIcon() {
            return icon;
        }

        /**
         * 問題情報を取得する
         * @return 問題情報( 問1 平成**年度 *期 )
         */
        public int getQ_info() {
            return q_info;
        }

        /**
         * 問題名を取得する
         * @return 問題名
         */
        public String getQ_summary() {
            return q_summary;
        }
    }

    /**
     * 指定問題IDの年度・回次を取得する
     * @param q_id 問題ID
     * @return 年度・回次情報
     */
    private question_info_data get_year_turn(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_year_turn(q_id), null);
        question_info_data questionInfoData = new question_info_data();

        if (c.moveToNext()) {
            questionInfoData.setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            questionInfoData.setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            c.close();
            db.close();
            return questionInfoData;
        }
        c.close();
        db.close();
        return null;
    }

    /**
     * 問題IDから問題名を取得する
     * @param q_id_data 	問題IDを持つ模擬試験情報
     * @return 問題名の配列
     */
    private String[] get_q_names(trial_exam_info_data[] q_id_data){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        String[] out_q_name = new String[q_id_data.length];
        db.beginTransaction();
        for( int i = 0; i < q_id_data.length; i++ ){
            Cursor c = null;
            c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_name(q_id_data[i].getQ_id()), null);
            if (c.moveToNext()) {
                out_q_name[i] = new String();
                out_q_name[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NAME));
            }
            c.close();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return out_q_name;
    }

    /**
     * 模擬試験情報を取得する
     * 取得した情報は模擬試験結果画面の表示と各種データベースの更新処理
     * に使われる
     *
     * @return 模擬試験情報
     */
    private trial_exam_info_data[] get_trial_exam_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.beginTransaction();
        Cursor c = db.rawQuery(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.get_trial_exam_info(), null);

        trial_exam_info_data[] trialExamInfoDatas = new trial_exam_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            trialExamInfoDatas[i] = new trial_exam_info_data();
            trialExamInfoDatas[i].setQ_id(c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.Q_ID)));
            trialExamInfoDatas[i].setUser_answer(c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER)));
            trialExamInfoDatas[i].setCorrect_mistake(c.getInt(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.CORRECT_MISTAKE)));
            trialExamInfoDatas[i].setUser_answer_date(c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER_DATE)));
            trialExamInfoDatas[i].setElapsed_time(c.getInt(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.ELAPSED_TIME)));
            i++;
        }

        c.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

        return trialExamInfoDatas;
    }

    /**
     * 成績情報にレコードを追加する
     * @param peformance_id		成績ID
     * @param user_answer_date	回答日時
     * @param peformance		成績
     * @param year 年度
     * @param turn 回次
     */
    private void add_performance_info(String peformance_id,String user_answer_date,int peformance,String year,String turn){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.execSQL(DB_SqlMaker.TBL_PERFORMANCE_INFO.add_performance_info(peformance_id, user_answer_date, peformance,year , turn));
        db.close();
    }

    /**
     * 成績情報のレコード数を取得する
     * @return レコード数
     */
    private int get_performance_id_count(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        int performance_id_count = 0;
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_PERFORMANCE_INFO.get_performance_id_count(), null);
        if (c.moveToNext()) {
            performance_id_count = c.getInt(c.getColumnIndex(DB_ColumnNames.PERFORMANCE_INFO_COUNT.ID));
        }
        c.close();
        db.close();

        return performance_id_count;
    }

    /**
     * 回答履歴テーブルのレコード数を取得する
     * @return レコード数
     */
    private int get_user_answer_id_count(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        int answer_id_count = 0;
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_ANSWER_ARCHIVE.get_user_answer_id_count(), null);
        if (c.moveToNext()) {
            answer_id_count = c.getInt(c.getColumnIndex(DB_ColumnNames.ANSWER_ARCHIVE_COUNT.ID));
        }
        c.close();
        db.close();

        return answer_id_count;
    }

    /**
     * サーバ送信情報テーブルのレコード数を取得する
     * @return レコード数
     */
    private int get_send_id_count(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        int send_id_count = 0;
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_SERVER_SEND_INFO.get_send_id_count(), null);
        if (c.moveToNext()) {
            send_id_count = c.getInt(c.getColumnIndex(DB_ColumnNames.SERVER_SEND_INFO_COUNT.ID));
        }
        c.close();
        db.close();

        return send_id_count;
    }

    /**
     * 回答履歴テーブルとサーバ送信情報テーブルにレコードを追加する
     * @param trialExamInfoDatas 模擬試験情報データ
     */
    private void add_answer_archive_and_send_info(trial_exam_info_data[] trialExamInfoDatas){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        //回答履歴情報テーブルのレコード数を取得する
        int user_answer_id_count = get_user_answer_id_count();

        //サーバ送信情報テーブルのレコード数を取得する
        int send_id_count = get_send_id_count();

        //トランザクション開始
        db.beginTransaction();
        int j = 1;
        for (int i = 0; i < trialExamInfoDatas.length; i++) {

            if (trialExamInfoDatas[i].getUser_answer() != null) {
                String new_answer_id = "A" + String.format("%05d", user_answer_id_count + j);
                String new_send_id = "S" + String.format("%03d", send_id_count + j);
                j++;
                db.execSQL(DB_SqlMaker.TBL_ANSWER_ARCHIVE.add_answer_archive_info(new_answer_id, trialExamInfoDatas[i].getQ_id(), trialExamInfoDatas[i].getUser_answer(), trialExamInfoDatas[i].getCorrect_mistake(), trialExamInfoDatas[i].getUser_answer_date()));
                db.execSQL(DB_SqlMaker.TBL_SERVER_SEND_INFO.add_server_send_info(new_send_id, new_answer_id, trialExamInfoDatas[i].getUser_answer_date()));
            }
        }
        //トランザクション成功を宣言。
        db.setTransactionSuccessful();
        // トランザクション終了
        db.endTransaction();
        db.close();
    }

    /**
     * 間違い回数をインクリメントする
     * @param trialExamInfoDatas 模擬試験情報(問題IDのリストを持っている)
     */
    private void increment_mistake_count(trial_exam_info_data[] trialExamInfoDatas){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.beginTransaction();
        for(int i = 0; i < trialExamInfoDatas.length; i++) {
            if ( trialExamInfoDatas[i].getCorrect_mistake() == 0 && trialExamInfoDatas[i].getUser_answer() != null ) {
                db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.increment_mistake_count(trialExamInfoDatas[i].getQ_id()));
            }
        }
        //トランザクション成功を宣言。
        db.setTransactionSuccessful();
        // トランザクション終了
        db.endTransaction();
        db.close();
    }

}

package com.support.qualifications.getqualifications.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapText;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.question_info_data;
import com.support.qualifications.getqualifications.database.review_date_data;
import com.support.qualifications.getqualifications.processing.ScrollValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * 復習モード
 */
public class Activity_Mode_Review extends Activity_Base implements CompoundButton.OnCheckedChangeListener{

    //表示する問題数
    public static final int QUESTION_LIMIT_NUM = 500;

    /** モードID */
    private int mode_id;

    /** リストビューのスクロール位置 */
    public static ScrollValue scrollValue =  new ScrollValue();

    /** ランダムモード(true:ランダムモード,false:通常モード) */
    private boolean isRandomMode;

    /** 間違い回数順の問題情報リスト */
    private question_info_data[] mistake_count_q_i_data;

    /** 日付順の問題情報リスト */
    private question_info_data[] date_q_i_data;

    /** 間違い回数順の問題IDリスト */
    private String[] mistake_qid_data;

    /** 日付順の問題IDリスト */
    private String[] date_qid_data;

    /** ラジオボタングループ */
    private RadioGroup review_radioGroup;

    /** リストビュー */
    private ListView myListview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_review);

        //自動スクロール位置初期化
        scrollValue.setScrollposition(0);

        //リストビュー取得
        myListview = (ListView)findViewById(R.id.listview_mode_review);

        //ナビゲーションドロワーセット
        setNavigation((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_mode_review));

        //intentからデータ取得
        getIntentValue();

        //ランダムスイッチにチェンジリスナーをセット
        ((Switch)findViewById(R.id.review_random_switch)).setOnCheckedChangeListener(this);

        //RadioButtonのセット
        setRadioButton();

    }

    @Override
    protected void onResume() {
        super.onResume();

        //リストビューに表示するデータをセット
        setQuestionInfoList();

        //ListViewセット
        setListViewforRadio();

        //今まで解いていた問題の場所まで自動スクロール
        if(!isRandomMode) {
            final ListView myListView = (ListView) findViewById(R.id.listview_mode_review);
            myListView.post(new Runnable() {
                @Override
                public void run() {
                    myListView.smoothScrollToPositionFromTop(scrollValue.getScrollposition(), 0, 400);
                }
            });
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked == true){
            //ランダムモードon
            isRandomMode = true;
        }else{
            //ランダムモードoff
            isRandomMode = false;
        }
    }
    /**
     * Intentからデータを取得する
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
        ((TextView)findViewById(R.id.mode_review_title)).setText(intent.getStringExtra(IntentKeyword.TOOLBER_TITLE));
    }

    /**
     * 問題情報リストをセットしておく
     */
    private void setQuestionInfoList(){
        mistake_count_q_i_data = get_list_q_i_info_forMistakeCount();
        date_q_i_data = get_list_q_i_info_forDate();
    }

    /**
     * ラジオボタンの設定を行う
     */
    private void setRadioButton(){

        review_radioGroup = (RadioGroup) findViewById(R.id.review_radiogroup);
        review_radioGroup.check(R.id.review_radiobutton_mistake_count);
        review_radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                final int id = radioButton.getId();
                switch (id) {
                    case R.id.review_radiobutton_mistake_count:
                        if (radioButton.isChecked()) {
                            myListview.removeAllViewsInLayout();
                            setListView(getUsers(mistake_count_q_i_data,id) , mistake_qid_data);
                        }
                        break;
                    case R.id.review_radiobutton_date:
                        if (radioButton.isChecked()) {
                            myListview.removeAllViewsInLayout();
                            setListView(getUsers(date_q_i_data,id) ,date_qid_data);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }



    /**
     * ListVIewをセットする
     */
    private void setListViewforRadio() {

        final int id = review_radioGroup.getCheckedRadioButtonId();
        switch (id) {
            case R.id.review_radiobutton_mistake_count:
                setListView(getUsers(mistake_count_q_i_data,id),mistake_qid_data);
                break;
            case R.id.review_radiobutton_date:
                setListView(getUsers(date_q_i_data,id),date_qid_data);
                break;
            default:
                break;
        }
    }
    /**
     * ListViewに表示するデータの定義クラス(User)のArrayListを作成し
     * その結果を返す
     * @param q_i_data セットする問題情報リスト
     * @return UserのArrayList
     */
    private ArrayList<User> getUsers(question_info_data[] q_i_data,int id){

        ArrayList<User> users = new ArrayList<>();

        for (int i = 0; i < q_i_data.length; i++) {
            User user = new User();
            user.setQ_info(getQ_info(q_i_data[i]));
            user.setQ_summary(q_i_data[i].getQ_name());
            switch (id) {
                case R.id.review_radiobutton_mistake_count:
                    user.setAdd_info(new BootstrapText.Builder(getApplicationContext()).addText((q_i_data[i].getMistake_count())+ " ").addText("回").build());
                    break;
                case R.id.review_radiobutton_date:
                    String formatDate = getFormatedDate(((review_date_data)q_i_data[i]).getUser_answer_date());
                    user.setAdd_info(new BootstrapText.Builder(getApplicationContext()).addFontAwesomeIcon("fa-clock-o").addText(" " + formatDate).build());
                    break;
                default:
                    break;
            }

            users.add(user);
        }
        return users;
    }

    /**
     * 日付データをフォーマットして返す
     * @param date 日付データ
     * @return フォーマット後日付データ
     */
    private String getFormatedDate(String date){
        return Integer.valueOf(date.substring(5, 7)) + "/" + Integer.valueOf(date.substring(8, 10)) + " " + date.substring(10, 16);
    }

    /**
     * レストビューの問題情報として表示する文字列を取得する
     *
     * @param q_i_data 問題情報
     * @return リストビューに表示する問題情報
     */
    private String getQ_info(question_info_data q_i_data){
        return q_i_data.getYear() + " " + q_i_data.getTurn() + " 問 " + String.format("%02d", q_i_data.getQ_no());
    }


    /**
     * ListViewの設定を行う
     * @param users　ListViewに表示するデータ
     */
    private void setListView( ArrayList<User> users ,final String[] q_id_data){

        UserAdapter adapter = new UserAdapter(this, 0, users);
        myListview.setEmptyView(findViewById(R.id.review_empty));
        myListview.setAdapter(adapter);
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {
                //選択した問題回答画面へ
                goSelectQuestionActivity(position,q_id_data);
            }
        });
    }

    /**
     * 問題IDリストをシャッフルする
     * 選択した問題の問題IDは先頭に配置する
     * @param position 選択した問題の場所
     * @param q_id_list シャッフルする問題IDリスト
     * @return シャッフルした問題IDリスト
     */
    private String[] getShuffle_qid_data(int position , String[] q_id_list){
        //配列からlistへ変換
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(q_id_list));
        //選択した問題のq_idを退避
        String first_q_id = list.get(position);
        list.remove(position);
        //リストの並びをシャッフル
        Collections.shuffle(list);
        //退避したq_idを先頭に追加
        list.add(0, first_q_id);
        //シャッフルしたデータを返す
        return (String[])list.toArray(new String[list.size()]);
    }

    /**
     * ListViewで選択した問題の回答画面に画面遷移する処理を行う
     * @param position 選択した問題の場所
     */
    private void goSelectQuestionActivity(int position , String[] q_id_data){

        Intent intent = new Intent(Activity_Mode_Review.this, Activity_Question.class);

        intent.putExtra(IntentKeyword.MODE_ID, mode_id);

        //ランダムモード
        if(isRandomMode) {
            intent.putExtra(IntentKeyword.SELECT_Q_ID, 0);
            //シャッフルした問題IDリストを渡す
            intent.putExtra(IntentKeyword.Q_ID_LIST, getShuffle_qid_data(position,q_id_data));
        }else{
            intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
            intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        }
        startActivity(intent);
    }
    /**
     * 復習モードに必要な問題情報リストを間違い回数順に取得する
     * @return 問題情報
     */
    private question_info_data[] get_list_q_i_info_forMistakeCount(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_list_q_i_info_forMistakeCount(QUESTION_LIMIT_NUM), null);
        mistake_qid_data = new String[c.getCount()];
        question_info_data[] questionInfoDatas = new question_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            mistake_qid_data[i] = new String(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID)));
            questionInfoDatas[i] = new question_info_data();
            questionInfoDatas[i].setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            questionInfoDatas[i].setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            questionInfoDatas[i].setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
            questionInfoDatas[i].setQ_name(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NAME)));
            questionInfoDatas[i].setMistake_count(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.MISTAKE_COUNT)));
            i++;
        }
        c.close();
        db.close();
        return questionInfoDatas;
    }

    /**
     * 復習モードに必要な問題情報リストを日付順に取得する
     * @return 問題情報
     */
    private question_info_data[] get_list_q_i_info_forDate(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = db.rawQuery(DB_SqlMaker.get_list_q_i_info_forDate(), null);

        //問題IDリスト
        ArrayList<String> q_id_list = new ArrayList<>();

        //問題情報リスト
        ArrayList<review_date_data> q_info_list = new ArrayList<>();

        while (c.moveToNext()) {

            //出題数制限
            if(q_id_list.size() >= QUESTION_LIMIT_NUM){
                break;
            }

            String q_id = new String(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID)));

            //一度出た問題IDの問題情報は登録しない
            if(!q_id_list.contains(q_id)) {
                q_id_list.add(q_id);
                review_date_data reviewDateData = new review_date_data();
                reviewDateData.setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
                reviewDateData.setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
                reviewDateData.setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
                reviewDateData.setQ_name(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NAME)));
                reviewDateData.setUser_answer_date(c.getString(c.getColumnIndex(DB_ColumnNames.ANSWER_ARCHIVE.USER_ANSWER_DATE)));
                q_info_list.add(reviewDateData);
            }
        }

        //list→配列変換
        date_qid_data = q_id_list.toArray(new String[q_id_list.size()]);
        c.close();
        db.close();
        return q_info_list.toArray(new review_date_data[q_info_list.size()]);
    }



    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            if( convertview == null ){
                convertview = layoutInflater.inflate(R.layout.question_list_item_review, parent, false);
                holder = new ViewHolder();
                holder.info = (com.beardedhen.androidbootstrap.BootstrapLabel) convertview.findViewById(R.id.review_questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.review_questionsummary);
                holder.addinfo = (com.beardedhen.androidbootstrap.AwesomeTextView)convertview.findViewById(R.id.add_info);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }
            User user = (User)getItem(pos);
            holder.info.setBootstrapText(new BootstrapText.Builder(getContext()).addText(user.getQ_info()).build());
            holder.summary.setText(user.getQ_summary());
            holder.addinfo.setBootstrapText(user.getAdd_info());
            return convertview;

        }
    }

    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** 問題情報( 問1など )を表示するテキスト */
        com.beardedhen.androidbootstrap.BootstrapLabel info;

        /** 問題名を表示するテキスト */
        TextView summary;

        com.beardedhen.androidbootstrap.AwesomeTextView addinfo;
    }

    /**
     * ListViewに表示するデータの定義クラス
     */
    private class User{

        /** 問題情報( 問1など ) */
        private String q_info;

        /** 問題名 */
        private String q_summary;

        /** 間違い回数や日付 */
        private BootstrapText add_info;


        /**
         * 問題情報を格納する
         * @param q_info 問題情報( 問1 平成**年度 *期 )
         */
        public void setQ_info(String q_info) {
            this.q_info = q_info;
        }

        /**
         * 問題名を格納する
         * @param q_summary 問題名
         */
        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        /**
         * 問題情報を取得する
         * @return 問題情報
         */
        public String getQ_info() {
            return q_info;
        }

        /**
         * 問題名を取得する
         * @return 問題名
         */
        public String getQ_summary() {
            return q_summary;
        }

        public BootstrapText getAdd_info() {
            return add_info;
        }

        public void setAdd_info(BootstrapText add_info) {
            this.add_info = add_info;
        }
    }
}

package com.support.qualifications.getqualifications.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AiteaOpenHelper extends SQLiteOpenHelper {

    private static final String DB_FILE_NAME = "aitea_ap_160325.sqlite3";
    private static final String DB_NAME = "160325_01_aitea.db";
    private static final int DB_VERSION = 1;

    private Context context;
    private File dbPath;
    private boolean DatabaseExit = false;

    public AiteaOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        this.dbPath = context.getDatabasePath(DB_NAME);
    }

    /* データベースをread-onlyで開く */
    @Override
    public synchronized SQLiteDatabase getReadableDatabase() {

        SQLiteDatabase database = super.getReadableDatabase();

        /* データベースがいたら、assets内のdbファイルにコピー */
        if (DatabaseExit) {
            try {
                database = copyDatabase(database);
            } catch (IOException e) {
            }
        }
        return database;
    }

    /* データベースをwright-onlyで開く */
    @Override
    public synchronized SQLiteDatabase getWritableDatabase() {

        SQLiteDatabase database = super.getWritableDatabase();

        /* データベースがいたら、assets内のdbファイルにコピー */
        if (DatabaseExit) {
            try {
                database = copyDatabase(database);
            } catch (IOException e) {
            }
        }
        return database;
    }

    private SQLiteDatabase copyDatabase(SQLiteDatabase database) throws IOException {

        //dbを閉じ、書き換えができる状態にする。
        database.close();

        // dbをコピー
        InputStream input = context.getAssets().open(DB_FILE_NAME);
        OutputStream output = new FileOutputStream(this.dbPath);
        copy(input, output);

        DatabaseExit = false;

        // dbを閉じたのでまた開く
        return super.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onOpen(db);

        this.DatabaseExit = true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    // CopyUtilsからのコピペ
    private static int copy(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024 * 4];
        int count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

}
package com.support.qualifications.getqualifications.database;

/**
 * 問題情報テーブル( tbl_question_info )のデータ型クラスに回答日時を追加したクラス
 */
public class review_date_data extends question_info_data {

    /** 回答日時( yyyy-mm-dd hh:mm:ss ) */
    private String user_answer_date;

    public String getUser_answer_date() {
        return user_answer_date;
    }

    public void setUser_answer_date(String user_answer_date) {
        this.user_answer_date = user_answer_date;
    }
}

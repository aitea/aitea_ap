package com.support.qualifications.getqualifications.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.support.qualifications.getqualifications.R;
import com.support.qualifications.getqualifications.common.AiteaMode;
import com.support.qualifications.getqualifications.common.AiteaPreference;
import com.support.qualifications.getqualifications.common.IntentKeyword;
import com.support.qualifications.getqualifications.database.AiteaOpenHelper;
import com.support.qualifications.getqualifications.database.DB_ColumnNames;
import com.support.qualifications.getqualifications.database.DB_SqlMaker;
import com.support.qualifications.getqualifications.database.genre_info_data;
import com.support.qualifications.getqualifications.processing.Analyze_Calculate;
import com.support.qualifications.getqualifications.processing.q_rate_info;
import com.support.qualifications.getqualifications.processing.set_q_rate_infoComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class Activity_Base extends FragmentActivity {

    /**
     * 画面遷移時のオプションフラグ
     * 初期値は、スタックに残っているアクティビティをすべて削除するフラグ
     */
    private int intentFlag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;

    /**
     * ナビゲーションドロワーをセットする
     * @param toolbar セット先のツールバー
     */
    protected void setNavigation(Toolbar toolbar){
        final DrawerLayout _DrawerLayout = (DrawerLayout)findViewById(R.id._DrawerLayout);


        Drawable drawable = getResources().getDrawable(R.drawable.icon_drawer);
        //画像のあるパスからdrawableを生成
        Bitmap orgBitmap = ((BitmapDrawable)drawable).getBitmap();
        //DrawableからBitmapインスタンスを取得
        final double resizeRate = 0.3;
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(orgBitmap,(int)(orgBitmap.getWidth() * resizeRate), (int)(orgBitmap.getHeight() * resizeRate), false);
        //drawrableからBitmapに変換
        drawable = new BitmapDrawable(getResources(), resizedBitmap);

        toolbar.setNavigationIcon(drawable);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _DrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        NavigationView _navigation = (NavigationView)findViewById(R.id._navigattion);

        _navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int menuId = menuItem.getItemId();

                //模擬試験はアラートダイアログを出すため、すぐにはホームへ戻る処理をしない
                if(menuId != R.id.drawer_menu_trial_exam ){
                    goHome();
                }

                switch (menuId) {

                    //ホーム
                    case R.id.drawer_menu_home:
                        break;

                    //aitea
                    case R.id.drawer_menu_aitea:
                        goAiteaMode();
                        break;

                    //模擬試験
                    case R.id.drawer_menu_trial_exam:
                        goTrialExam();
                        break;

                    //年度別
                    case R.id.drawer_menu_year:
                        goDictionary_Year(AiteaMode.YEAR);
                        break;

                    //ジャンル別
                    case R.id.drawer_menu_genre:
                        goDictionaryGenre();
                        break;

                    //復習
                    case R.id.drawer_menu_review:
                        goReview();
                        break;

                    //お気に入り
                    case R.id.drawer_menu_favorite:
                        goFavorite();
                        break;

                    //分析結果
                    case R.id.drawer_menu_analyze_result:
                        goAnalyzeResult();
                        break;

                    //統計データ
                    case R.id.drawer_menu_statistical_info:
                        goStatistical_information();
                        break;

                    //克服リスト
                    case R.id.drawer_menu_overcome_list:
                        goOvercome();
                        break;

                    //設定
                    case R.id.drawer_menu_setting:
                        goSetting();
                        break;

                }
                return true;
            }
        });
    }
    /**
     * ナビゲーションドロワーを閉じる
     */
    protected void closeNavigation(){
        DrawerLayout _DrawerLayout = (DrawerLayout)findViewById(R.id._DrawerLayout);
        _DrawerLayout.closeDrawer(Gravity.LEFT);
    }

    /**
     * インテントのフラグをセットする
     * @param flag
     */
    protected void setIntentFlag(int flag){
        intentFlag = flag;
    }

    /**
     * ホーム画面に遷移する
     */
    private void goHome(){
        Intent intent = new Intent(getApplication(),Activity_Main.class);
        intent.setFlags(intentFlag);
        startActivity(intent);
    }

    /**
     * aitea画面に遷移する
     */
    protected void goAiteaMode(){
        //小ジャンルの正答率を更新する
        update_s_class_correct_rate();

        //出題確率を元に問題IDリストを取得
        String[] q_id_data = get_q_id_by_set_q_rate();
        Intent intent = new Intent(getApplication(),Activity_Question.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.AITEA);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, 0);
        startActivity(intent);
    }

    /**
     * 模擬試験画面に遷移する
     */
    protected void goTrialExam(){
        //模擬試験から問題IDを取得する
        String[] q_id_data = get_trial_exam_q_id();

        if (q_id_data == null) {
            //年度別へ
            goDictionary_Year(AiteaMode.TRIALEXAM);
        } else {
            //途中から模擬試験モードを開始するアラート表示
            showRestartTrialExamAlertDialog(q_id_data);
        }
    }

    /**
     * 模擬試験問題リスト画面に遷移
     * @param q_id_data 問題IDリスト
     */
    protected void goTrialExam_List(String[] q_id_data){
        goHome();
        Intent intent = new Intent(getApplication(),Activity_Mode_TrialExam_List.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.TRIALEXAM);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        startActivity(intent);
    }

    /**
     * 年度別画面に遷移する
     * @param mode モードID
     */
    protected void goDictionary_Year(int mode){
        if(mode == AiteaMode.TRIALEXAM){
            goHome();
        }
        Intent intent = new Intent(getApplication(),Activity_Mode_Dictionary_Year.class);
        intent.putExtra(IntentKeyword.MODE_ID, mode);
        startActivity(intent);
    }
    /**
     * ジャンル別画面に遷移する
     */
    protected void goDictionaryGenre(){
        Intent intent = new Intent(getApplication(),Activity_Mode_Dictionary_Genre.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.GENRE);
        startActivity(intent);
    }

    /**
     * 復習画面に遷移する
     */
    protected void goReview(){
        //間違い回数を元に問題IDを取得
        Intent intent = new Intent(getApplication(), Activity_Mode_Review.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.REVIEW);
        intent.putExtra(IntentKeyword.TOOLBER_TITLE, getString(R.string.title_activity_mode_review));
        startActivity(intent);
    }

    /**
     * お気に入り画面に遷移する
     */
    protected void goFavorite(){
        Intent intent = new Intent(getApplication(), Activity_Mode_Favorite.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.FAVORITE);
        intent.putExtra(IntentKeyword.TOOLBER_TITLE, getString(R.string.title_activity_mode_favorite));
        startActivity(intent);
    }
    /**
     * 試験結果画面(統計)に遷移する
     */
    protected void goStatistical_information(){
        Intent intent = new Intent(getApplication(), Activity_OptionMenu_Statistical_information.class);
        startActivity(intent);
    }

    /**
     * 分析結果画面に遷移する
     */
    protected void goAnalyzeResult(){
        Intent intent = new Intent(getApplication(), Activity_OptionMenu_Analyze_Result.class);
        startActivity(intent);
    }

    /**
     * 克服一覧画面に遷移する
     */
    protected void goOvercome(){
        Intent intent = new Intent(getApplication(), Activity_OptionMenu_Overcome_List.class);
        intent.putExtra(IntentKeyword.MODE_ID, AiteaMode.OVERCOME);
        intent.putExtra(IntentKeyword.TOOLBER_TITLE, getString(R.string.title_activity_mode_overcome));
        startActivity(intent);
    }

    /**:
     * 設定画面に遷移する
     */
    protected void goSetting(){
        Intent intent = new Intent(getApplication(), Activity_OptionMenu_Setting.class);
        startActivity(intent);
    }

    /**
     * 指定テーブルの全レコードを削除する
     * @param table_name 指定テーブル名
     */
    protected void delete_tbl_info(String table_name){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.delete(table_name, null, null);
        db.close();
    }

    /**
     * 模擬試験を前回の続きからはじめるかユーザに問う
     * @param q_id_data 模擬試験の問題IDリスト
     */
    private void showRestartTrialExamAlertDialog(final String[] q_id_data){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.alert_title_trial_exam_restart));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_trial_exam_restart));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //模擬試験問題リストへ遷移
                goTrialExam_List(q_id_data);
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //模擬試験情報テーブルの全レコード削除
                delete_tbl_info(DB_ColumnNames.TRIAL_EXAM_INFO.TABLE_NAME);
                //年度別へ遷移
                goDictionary_Year(AiteaMode.TRIALEXAM);
            }
        });
        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    /**
     * 弱点分析し、出題確率を元に問題IDリストを取得
     * @return 問題IDリスト
     */
    private String[] get_q_id_by_set_q_rate(){

        // 全てのジャンルの正答率を取得する
        ArrayList<q_rate_info> q_rate_infos = get_all_genre_correct_rate();

        // 正答率が低いもの順にソート
        Collections.sort(q_rate_infos, new set_q_rate_infoComparator());

        // 各ジャンルに正答率についての順位を付ける(正答率が低いものが順位が高い)
        Analyze_Calculate.set_rank(q_rate_infos);

        // 出題確率を計算する
        Analyze_Calculate.calc_set_q_rate_infos(q_rate_infos);

        // 出題確率を元に問題idを取得する
        return select_q_id_by_q_rate(q_rate_infos);
    }


    /**
     * 全問題IDリストを持つHashmapを引数として受け取り、分野コード、大・中・小分類コードから
     * keyを作成し、keyに該当する問題IDをHashmapから取得し返す。
     *
     * @param map 全問題IDリストを持つHashmap
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @param s_class_code 小分類コード
     * @return 問題ID
     */
    private String get_q_id( HashMap<String,ArrayList<String>> map ,int field_code,int l_class_code,int m_class_code,int s_class_code){

        //keyを作成
        String key = String.valueOf(field_code) + "_" +
                String.valueOf(l_class_code) + "_" +
                String.valueOf(m_class_code) + "_" +
                String.valueOf(s_class_code);

        //問題IDが枯渇
        if(map.get(key).size() == 0) {
            return null;
        }

        //Hashmapからランダムに問題ID取得
        Random rnd = new Random();
        int rand_index = rnd.nextInt(map.get(key).size());
        String q_id = map.get(key).get(rand_index);

        //取得した問題IDをHashmapから除外
        map.get(key).remove(rand_index);

        return q_id;
    }


    /**
     * 克服ボタン押下時のアラートダイアログ表示
     */
    protected void ClickOvercomeAlertShow(){

        //以後表示しないをチェックしていた場合
        if(getOvercomeAlertShowFlag()){
            return;
        }

        LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.dialog,(ViewGroup) findViewById(R.id.alertdialog_layout));

        //以後表示しないチェックボックス処理
        CheckBox checkBox = (CheckBox) layout.findViewById(R.id.overcome_alert_checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setOvercomeAlertShowFlag(isChecked);
            }
        });

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.alert_title_overcome_register));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_overcome_register));
        alertDialogBuilder.setView(layout);

        alertDialogBuilder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    /**
     * 克服についての説明をするアラート表示するかどうかのフラグをセット
     * @param bool true:以後表示しない , false:表示する
     */
    protected void setOvercomeAlertShowFlag(boolean bool){

        SharedPreferences pref = getSharedPreferences(AiteaPreference.PREFNAME, MODE_PRIVATE);
        SharedPreferences.Editor e = pref.edit();
        e.putBoolean(AiteaPreference.OVERCOMEALERTFLAG, bool);
        e.commit();
    }

    /**
     * 克服についての説明をするアラート表示するかどうかのフラグを取得
     * @return true:以後表示しない , false:表示する
     */
    protected boolean getOvercomeAlertShowFlag(){
        SharedPreferences pref = getSharedPreferences(AiteaPreference.PREFNAME, MODE_PRIVATE);
        return pref.getBoolean(AiteaPreference.OVERCOMEALERTFLAG, false);
    }

    /**
     * ハッシュマップに含まれるアレイリストの要素数の合計を返す
     * @param map 対象ハッシュマップ
     * @return アレイリストの要素数
     */
    private int getMapValueSize(HashMap<String,ArrayList<String>> map){
        int ValueSize = 0;
        for(ArrayList<String> hoge : map.values()) {
            ValueSize += hoge.size();
        }
        return ValueSize;
    }
    /**
     * 広告のビューを設定する
     */
    protected void setAdView(AdView mAdView){
        //AdMobから広告読込み・表示
        //注意：デバッグ時はテストモード(addTestDevice)を使用すること(アカウント停止の可能性有り)
        //補足：テストモード(addTestDevice)の引数DeviceIDは、AndroidStudioのLogcatで「addTestDevice」で検索すると調べられる
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice("E2606A57860E602F023CCC8BB249F2A9").build();
        AdRequest adRequest = new AdRequest.Builder().build(); //実運用時はこちらを使用
        mAdView.loadAd(adRequest);
    }

    /**
     * 各小ジャンルがもつ出題確率を元に、出題する問題の問題IDリストを取得する。
     * @param q_rate_infos 出題確率とジャンル情報
     * @return  問題IDリスト
     */
    private String[] select_q_id_by_q_rate(ArrayList<q_rate_info> q_rate_infos){

        //取得する問題IDの数(最大)
        final int GET_QUESTION_NUM = 100;

        //全問題IDをHashmapで取得
        HashMap<String,ArrayList<String>> map = get_all_q_id_info();

        //問題ID数を取得する
        int GetQuestionNum = getMapValueSize(map);

        //取得する問題IDの最大を超えていたら最大に合わせる
        //克服フラグを大量に立てていたらGET_QUESTION_NUM分の問題をとってこれない可能性がある
        if(GetQuestionNum > GET_QUESTION_NUM){
            GetQuestionNum = GET_QUESTION_NUM;
        }

        String[] q_id_data= new String[GetQuestionNum];

        int i = 0;

        //枯渇したジャンルの確率を蓄積する
        double q_rate_ofset = 0;

        while( i < GetQuestionNum ) {

            //出題確率の蓄積値
            double sum_q_rate = 0 + q_rate_ofset;

            //0～0.999999…の範囲のランダム値を取得
            double rand = Math.random();

            for (int j = 0; j < q_rate_infos.size(); j++) {

                //該当ジャンルの出題確率を取得
                double set_q_rate = q_rate_infos.get(j).getSet_q_rate();

                //ランダム値が ( 出題確率の蓄積値 ～ 出題確率の蓄積値 + 該当ジャンルの出題確率 )の範囲内
                if ( ( rand >= sum_q_rate ) && ( rand < ( sum_q_rate + set_q_rate ) ) ) {
                    //該当ジャンルの問題IDを取得
                    String get_q_id = get_q_id( map,q_rate_infos.get(j).getField_code(),q_rate_infos.get(j).getL_class_code(),q_rate_infos.get(j).getM_class_code(),q_rate_infos.get(j).getS_class_code());

                    //該当ジャンルの問題IDが枯渇している
                    if( get_q_id == null) {
                        //出題確率を保持して、ジャンルをリストから除外
                        q_rate_ofset += q_rate_infos.get(j).getSet_q_rate();
                        q_rate_infos.remove(j);
                        break;
                    }
                    else{
                        q_id_data[i] = get_q_id;
                        i++;
                        break;
                    }
                } else {
                    //出題確率の蓄積値に該当ジャンルの出題確率を加える
                    sum_q_rate += set_q_rate;
                }
            }
        }
        return q_id_data;
    }
    /**
     * 小ジャンルの正答率を更新する
     */
    private void  update_s_class_correct_rate(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        ArrayList<genre_info_data> code_list = new ArrayList<>();

        db.beginTransaction();
        Cursor c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_all_genre_code_info(), null);
        while (c.moveToNext()) {
            genre_info_data genre_code = new genre_info_data();
            genre_code.setField_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_CODE)));
            genre_code.setL_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_CODE)));
            genre_code.setM_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_CODE)));
            genre_code.setS_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_CODE)));
            code_list.add(genre_code);
        }
        for(int i = 0; i < code_list.size() ; i++ ) {
            c = db.rawQuery(DB_SqlMaker.get_correct_info(
                            code_list.get(i).getField_code(),
                            code_list.get(i).getL_class_code(),
                            code_list.get(i).getM_class_code(),
                            code_list.get(i).getS_class_code(), Analyze_Calculate.AIM_ANS_COUNT),
                    null);
            int[] correct_info = new int[c.getCount()];
            int count = 0;
            while (c.moveToNext()) {
                correct_info[count] = c.getInt(c.getColumnIndex(DB_ColumnNames.ANSWER_ARCHIVE.CORRECT_MISTAKE));
                count++;
            }
            double[] molec_denomin = Analyze_Calculate.molec_denomin(correct_info);
            db.execSQL(DB_SqlMaker.TBL_GENRE_INFO.update_s_class_correct_rate(
                            code_list.get(i).getField_code(),
                            code_list.get(i).getL_class_code(),
                            code_list.get(i).getM_class_code(),
                            code_list.get(i).getS_class_code(),
                            molec_denomin,
                            getS_class_correct_rate(molec_denomin))
            );
        }
        c.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }
    /**
     * 引数で受け取った正答率の分子と分母を元に、
     * 正答率を演算し結果を返す
     * 正答率の分母が0の場合は0を返す
     * @param s_class_molec_denomin
     * @return 正答率
     */
    private double getS_class_correct_rate( double[] s_class_molec_denomin ){

        if( s_class_molec_denomin[1] == 0 ){
            return 0.0;
        }else {
            return s_class_molec_denomin[0] / s_class_molec_denomin[1];
        }
    }

    /**
     * 模擬試験情報テーブルから問題IDを取得する
     * @return 問題IDリスト
     */
    private String[] get_trial_exam_q_id(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.get_trial_exam_q_id(), null);

        if(c.getCount() == 0){
            return null;
        }
        String[] q_id_data = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            q_id_data[i] = new String();
            q_id_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();
        return q_id_data;
    }

    /**
     * 問題情報から問題IDと分野コードと大・中・小分類コードを取得する
     * (戻り値のHashmapの例)
     *  分野コード = 1,大分類コード = 2,中分類コード = 3,小分類コード = 4で
     *  該当する問題IDは"Q0001"の場合以下のようにデータ格納がされる
     * <"1_1_1_1","Q0001">
     *
     * @return 分野コードと大・中・小分類コードから問題IDを取得できるHashmap
     */
    private HashMap<String,ArrayList<String>> get_all_q_id_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();
        HashMap<String,ArrayList<String>> map = new HashMap<>();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_all_genre_q_id_info(), null);

        while (c.moveToNext()) {
            // keyをつくる
            String key = String.valueOf(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.FIELD_CODE))) + "_" +
                    String.valueOf(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.L_CLASS_CODE))) + "_" +
                    String.valueOf(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.M_CLASS_CODE))) + "_" +
                    String.valueOf(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.S_CLASS_CODE)));

            String q_id = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));

            // 初めて追加されるキー
            if(map.get(key) == null){
                //ArrayList新規作成
                ArrayList<String> list = new ArrayList<>();
                list.add(q_id);
                // マップ追加
                map.put(key, list);
            } else {
                // 既にあるキー
                map.get(key).add(q_id);
            }
        }
        c.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return map;

    }

    /**
     * 分野コード・大・中・小分類コードと小分類の正答率の取得
     * @return 分野コード・大・中・小分類コードと小分類の正答率をもつクラス
     */
    private ArrayList<q_rate_info> get_all_genre_correct_rate(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        ArrayList<q_rate_info> q_rate_infos = new ArrayList<>();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_all_genre_correct_rate(), null);

        while (c.moveToNext()) {
            q_rate_info qRateInfo = new q_rate_info();
            qRateInfo.setField_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_CODE)));
            qRateInfo.setL_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_CODE)));
            qRateInfo.setM_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_CODE)));
            qRateInfo.setS_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_CODE)));
            qRateInfo.setCorrect_rate(c.getDouble(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_CORRECT_RATE)));
            q_rate_infos.add(qRateInfo);
        }
        c.close();
        db.close();
        return q_rate_infos;
    }

}
